################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Kardome_V12_Core2.c 

SRC_OBJS += \
./src/Kardome_V12_Core2.doj 

C_DEPS += \
./src/Kardome_V12_Core2.d 


# Each subdirectory must supply rules for building sources it contributes
src/Kardome_V12_Core2.doj: ../src/Kardome_V12_Core2.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore SHARC C/C++ Compiler'
	cc21k -c -file-attr ProjectName="Kardome_V12_Core2" -proc ADSP-SC589 -flags-compiler --no_wrap_diagnostics -si-revision 1.0 -g -DCORE2 -D_DEBUG -DADI_MCAPI @includes-39ce2e44e4bde2d1f2ba6c77bb7b5e87.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -char-size-8 -swc -gnu-style-dependencies -MD -Mo "src/Kardome_V12_Core2.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


