################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/KRDM_separator.c \
../src/main_Core1.c 

SRC_OBJS += \
./src/KRDM_separator.doj \
./src/main_Core1.doj 

C_DEPS += \
./src/KRDM_separator.d \
./src/main_Core1.d 


# Each subdirectory must supply rules for building sources it contributes
src/KRDM_separator.doj: ../src/KRDM_separator.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore SHARC C/C++ Compiler'
	cc21k -c -file-attr ProjectName="Kardome_V12_Core1" -proc ADSP-SC589 -flags-compiler --no_wrap_diagnostics -si-revision 1.0 -g -DCORE1 -D_DEBUG -DADI_MCAPI @includes-b30d2b57289a7ab8660636f7f59616d9.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -char-size-8 -swc -gnu-style-dependencies -MD -Mo "src/KRDM_separator.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/main_Core1.doj: ../src/main_Core1.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore SHARC C/C++ Compiler'
	cc21k -c -file-attr ProjectName="Kardome_V12_Core1" -proc ADSP-SC589 -flags-compiler --no_wrap_diagnostics -si-revision 1.0 -g -DCORE1 -D_DEBUG -DADI_MCAPI @includes-b30d2b57289a7ab8660636f7f59616d9.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -char-size-8 -swc -gnu-style-dependencies -MD -Mo "src/main_Core1.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


