/*****************************************************************************
 * Kardome_V9_Core1.c
 *****************************************************************************/

#include <sys/platform.h>
#include <sys/adi_core.h>
#include "adi_initialize.h"

#include <services/int/adi_int.h>
#include <services/gpio/adi_gpio.h>

#include "KRDM_separator.h"




#include "..\..\Kardome_V12_Core0\src\GVL_parameters.h"


//#define GVL_NUM_AUDIO_CHANNELS          8
//#define GVL_SAMPLE_SIZE_BYTES      		4
//#define GVL_MAX_RX_BUFFER_SIZE_SAMPLES 	1024
//#define GVL_MAX_RX_BUFFER_SIZE_BYTES 	GVL_MAX_RX_BUFFER_SIZE_SAMPLES * GVL_SAMPLE_SIZE_BYTES * GVL_NUM_AUDIO_CHANNELS
//

//-------------------------------------------------
// 		Buffers shared with GVL
//-------------------------------------------------
int* 			p_GVL_RX_from_SPORT_PING 		= (int*)ADDRESS_RX_from_SPORT_PING;
int* 			p_GVL_RX_from_SPORT_PONG 		= (int*)ADDRESS_RX_from_SPORT_PONG;
int* 			p_GVL_TX_to_USB_PING 			= (int*)ADDRESS_TX_to_USB_PING;
int* 			p_GVL_TX_to_USB_PONG 			= (int*)ADDRESS_TX_to_USB_PONG;
int* 			p_USB_RX_from_USB_PING 			= (int*)ADDRESS_RX_from_USB_PING;
int* 	 		p_USB_RX_from_USB_PONG 			= (int*)ADDRESS_RX_from_USB_PONG;



/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

//void Interrupt_handler_PING(uint32_t Interrupt_ID, void* handlerArg);
//void Interrupt_handler_PONG(uint32_t Interrupt_ID, void* handlerArg);
void Interrupt_handler_from_ARM_core0(uint32_t Interrupt_ID, void* handlerArg);
void Interrupt_handler_from_ARM_core1(uint32_t Interrupt_ID, void* handlerArg);
void Interrupt_handler_from_ARM_core2(uint32_t Interrupt_ID, void* handlerArg);
void Interrupt_handler_from_ARM_core3(uint32_t Interrupt_ID, void* handlerArg);

int main(int argc, char *argv[])
{
	/**
	 * Initialize managed drivers and/or services that have been added to 
	 * the project.
	 * @return zero on success 
	 */
	adi_initComponents();
	
	/* Begin adding your custom code here */
	adi_int_InstallHandler(INTR_SOFT0, Interrupt_handler_from_ARM_core0,0,1);
	adi_int_InstallHandler(INTR_SOFT1, Interrupt_handler_from_ARM_core1,0,1);
	adi_int_InstallHandler(INTR_SOFT2, Interrupt_handler_from_ARM_core2,0,1);
	adi_int_InstallHandler(INTR_SOFT3, Interrupt_handler_from_ARM_core3,0,1);
	//----------------------------------------------------------
	// 		Initializations
	//----------------------------------------------------------
//	KRDM_init();
	//----------------------------------------------------------
	// 		Infinite loop
	//----------------------------------------------------------
	while(1)
	{
		asm("nop;");
	}
	return 0;
}

/*--------------------------------------------
 *  Interrupt_handler_from_ARM_core0
 * 	Alon Slapak 9.2.2019
 * 	Interrupt handler for SW interrupt from ARM core.
 * 	This handler is called when PING or PONG buffer is sent.
 *
 * 	Reference:
 * 		Project: 	ICC_Interrupt_Example1_Core.zip
 *		EZ thread: 	ADSP-SC58x/2158x interrupt SHARC core from ARM:Example code
 *		Link:		https://ez.analog.com/dsp/sharc-processors/adsp-sc5xxadsp-215xx/f/discussions/64082/adsp-sc58x-2158x-interrupt-sharc-core-from-arm-example-code
 *	Input:
 *		Interrupt_ID 	The ID of the interrupt
 *		handlerArg 		Callback parameters passed to the handler function
 * 	Output:
 *  	void
 *
 * 	Configuration of the RX/TX buffers:
 *   	Length(samples): 	SPORT_BUFFER_LENGTH_SAMPLES * 8 (channels)
 *   	Width: 				32 bit
 *   	Type: 				integer (20 bit left justified)
 * 		Arrangement: 		MIC1 - MIC8 (t), MIC1 - MIC8 (t+1), MIC1 - MIC8 (t+2), ...
 *  		| MIC1 | MIC2 | MIC3 | MIC4 | MIC5 | MIC6 | MIC7 | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 | MIC1 | ...
 *
 *
 * --------------------------------------------
 */
void Interrupt_handler_from_ARM_core0(uint32_t Interrupt_ID, void* handlerArg)
{
	//-------------------------------------------
	// 		PING buffer send
	//-------------------------------------------
	int k;
	for (k = 0; k < 256 * 8; k++)
	{
		p_GVL_TX_to_USB_PING[k] =  p_GVL_RX_from_SPORT_PING[k];
	}
}

void Interrupt_handler_from_ARM_core1(uint32_t Interrupt_ID, void* handlerArg)
{
	//-------------------------------------------
	// 		PONG buffer send
	//-------------------------------------------
	int k;
	for (k = 0; k < 256 * 8; k++)
	{
		p_GVL_TX_to_USB_PONG[k] =  p_GVL_RX_from_SPORT_PONG[k];
	}
}

void Interrupt_handler_from_ARM_core2(uint32_t Interrupt_ID, void* handlerArg)
{
	//-------------------------------------------
	// 		PING buffer send
	//-------------------------------------------
	int k;
	for (k = 0; k < SPORT_BUFFER_LENGTH_SAMPLES * GVL_NUM_AUDIO_CHANNELS; k++)
	{
		p_GVL_TX_to_USB_PING[k] =  p_GVL_RX_from_SPORT_PING[k];
	}
}

void Interrupt_handler_from_ARM_core3(uint32_t Interrupt_ID, void* handlerArg)
{
	//-------------------------------------------
	// 		PONG buffer send
	//-------------------------------------------
	int k;
	for (k = 0; k < SPORT_BUFFER_LENGTH_SAMPLES * GVL_NUM_AUDIO_CHANNELS; k++)
	{
		p_GVL_TX_to_USB_PONG[k] =  p_GVL_RX_from_SPORT_PONG[k];
	}
}
