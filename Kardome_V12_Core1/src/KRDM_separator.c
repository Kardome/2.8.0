/*
 * KRDM_separator.c
 *
 *  Created on: Mar 6, 2019
 *      Author: Kardome
 */

#ifndef KRDM_SEPARATOR_C_
#define KRDM_SEPARATOR_C_

#include "KRDM_separator.h"
//-------------------------------------------------
// 		Buffers
//-------------------------------------------------
complex_float* 	KRDM_BUFFER_BEFORE		= (complex_float*)KRDM_BUFFER_BEFORE_ADDRESS;
float* 			KRDM_BUFFER_AFTER		= (float*)KRDM_BUFFER_AFTER_ADDRESS;
complex_float* 	KRDM_BUFFER_FFT			= (complex_float*)KRDM_BUFFER_FFT_ADDRESS;
complex_float* 	KRDM_BUFFER_IFFT		= (complex_float*)KRDM_BUFFER_IFFT_ADDRESS;
//-------------------------------------------------
// 		Static variables
//-------------------------------------------------
static int 				KRDM_index_to_insert_new_data;
static int 				KRDM_index_extract_processed_data;
static int 				KRDM_sport_buffers_aggregated;

/*--------------------------------------------
 *  KRDM_init
 * 	Alon Slapak 6.3.2019
 * 	Initialize the Kardome algorithm
 *
 * 	Reference:
 *	Input:
 *		void
 * 	Output:
 *  	void
 *---------------------------------------------
 */
void KRDM_init()
{
	KRDM_index_to_insert_new_data 			= 0;
	KRDM_index_extract_processed_data 		= 0;
	KRDM_sport_buffers_aggregated 			= -1; // The first time should be +1 buffers
}
/*--------------------------------------------
 *  KRDM_operate
 * 	Alon Slapak 5.3.2019
 * 	Operating the Kardome algorithm.
 *
 *	Input:
 *		RX_buffer 	pointer to the buffer of the raw data
 *   			Length(samples): 	SPORT_BUFFER_LENGTH_SAMPLES * 8 (channels)
 *   			Width: 				32 bit
 *   			Type: 				integer (signed) 	TODO AS 5.3.2019 20 bit left justified
 * 				Arrangement: 		MIC1 - MIC8 (t), MIC1 - MIC8 (t+1), MIC1 - MIC8 (t+2), ...
 * 		TX_buffer 	pointer to the buffer of output data (same format as RX_buffer)
 * 				Length(samples): 	SPORT_BUFFER_LENGTH_SAMPLES * 8 (channels)
 *   			Width: 				32 bit
 *   			Type: 				integer (signed)	TODO AS 5.3.2019 20 bit left justified
 * 				Arrangement: 		MIC1 - MIC8 (t), MIC1 - MIC8 (t+1), MIC1 - MIC8 (t+2), ...
 * 	Output:
 *  	void
 *
 *---------------------------------------------
 */
void KRDM_operate(int* RX_buffer, int* TX_buffer)
{
	//---------------------------------------
	// 		RX_buffer --> BUFFER_BEFORE
	//---------------------------------------
	KRDM_interleave_to_buffer_before(RX_buffer);
	//---------------------------------------
	// 		KRDM algorithm (Once enough data was aggregated)
	//---------------------------------------
	KRDM_sport_buffers_aggregated++;
	if (KRDM_sport_buffers_aggregated >= KRDM_NUMBR_OF_NONOVERLAP_BUFFERS)
	{
		KRDM_sport_buffers_aggregated = 0;
		KRDM_pipeline();
	}
	//---------------------------------------
	// 		BUFFER_AFTER --> TX_buffer
	//---------------------------------------
	KRDM_buffer_after_to_interleave(TX_buffer);
}

/*--------------------------------------------
 *  KRDM_pipeline
 * 	Alon Slapak 12.3.2019
 * 	The pipeline of the Kardome algorithm.
 *
 *	Input:
 *		void
 * 	Output:
 *  	void
 *
 *  Pipeline:
 *  =========
 *  	Window		DMA core->accel 	FFT on accel 	DMA accel->core 	Core processing
 *  						D0  				-  					-  					-
 *  						D1  				D0  				-  					-
 *  						D2  				D1 				 	D0  				-
 *  						D3  				D2 					D1  				D0
 *  						D4  				D3  				D2 				 	D1
 *  						...  				...  				...  				...
 *  						Dn+2  				Dn+1  				Dn  				Dn-1
 *  						-  					-  					-  					Dn
 *  			MIC1	|	MIC2 	|	MIC3	|	MIC4	|	MIC5	|	MIC6	|	MIC7	|	MIC8
 *
 *  FFT
 *
 * 	2. 	if enough samples were aggregated
 * 	3. 		KRDM algorithm
 * 	3.1			for each microphone:
 * 	3.2				Windowed FFT
 * 	3.3				Spatial filtering
 * 	3.4				IFFT
 * 	3.5				Weighted Overlap add (WOLA)*
 *---------------------------------------------
 */
void KRDM_pipeline(void)
{
	int 				mic_number;
	complex_float* 		p_segment_to_FFT;

	//---------------------------------------
	// 		Process each microphone
	//---------------------------------------
	for (mic_number = 0; mic_number < KRDM_NUMBER_OF_MICS; mic_number++)
	{
		p_segment_to_FFT 	= (complex_float*)(&KRDM_BUFFER_BEFORE[KRDM_index_to_insert_new_data]);
		//---------------------------------------
		// 		FFT/IFFT
		// TODO: AS 11.3.2019 replace with FFT/IFFT
		//---------------------------------------
		memcpy((char*)KRDM_BUFFER_IFFT, (char*)p_segment_to_FFT, KRDM_FFT_BUFFER_LENGTH_SAMPLES * KRDM_COMPLEX_FLOAT_BYTES);
		//---------------------------------------
		// 		Overlap add
		//---------------------------------------
		KRDM_overlap_add(mic_number, KRDM_BUFFER_IFFT);
	}
	//----------------------------------------
	// 		Reset the BUFFER_AFTER index
	//----------------------------------------
	KRDM_index_extract_processed_data = 0;
}

/*--------------------------------------------
 *  KRDM_interleave_to_buffer_before
 * 	Alon Slapak 5.3.2019
 * 	Parsing the data from the interleave channels of the SPORT to a pseudo-circular array of MICS.
 *
 *	Input:
 *		RX_buffer 			pointer to the buffer of the raw data
 *   	Length(samples): 	SPORT_BUFFER_LENGTH_SAMPLES * 8 (channels)
 *   	Width: 				32 bit
 *   	Type: 				integer (signed)
 *   	 	TODO AS 5.3.2019 20 bit left justified
 * 		Arrangement: 		MIC1 - MIC8 (t), MIC1 - MIC8 (t+1), MIC1 - MIC8 (t+2), ...
 *  		| MIC1 | MIC2 | MIC3 | MIC4 | MIC5 | MIC6 | MIC7 | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 | MIC1 | ...
 *
 * 	Output:
 *  	void
 *
 * 	Algorithm:
 * 	----------
 * 	Pseudo-circular array (PCA):
 * 	The PCA is a circular array without modulus operation on the index (for example, when there is no circular DMA)
 * 	The idea is to maintain duplicate array of the aggregated sub-segments (|  t(x)  |), and to move the pointer of the beginning
 * 	of the processed segment (VV...VVVV). The duplicate array saves the need for cyclic DMA.
 * 	Note: When there is no cyclic DMA, the alternative is to shift all the past data by memcpy, and copy the new data to its place.
 * 	This means N copy operations. The PCA requires only N/2 copy instructions at the cost of double (X2) memory.
 * 	PROs:   1. No need for cyclic DMA
 * 	      	2. No need to shift past sub-segments
 * 	      	3. N/2 copy operations instead of N copy operations
 * 	CONs: 	1. X2 memory
 *
 *
 * 	 |  t(1)  |  ----  |  ----  |  ----  |  t(1)  |  ----  |  ----  |  ----  |
 *            VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(1)  |  t(2)  |  ----  |  ----  |  t(1)  |  t(2)  |  ----  |  ----  |
 *                     VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(1)  |  t(2)  |  t(3)  |  ----  |  t(1)  |  t(2)  |  t(3)  |  ----  |
 *                              VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *    *
 *   |  t(1)  |  t(2)  |  t(3)  |  t(4)  |  t(1)  |  t(2)  |  t(3)  |  t(4)  |
 *   VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(5)  |  t(2)  |  t(3)  |  t(4)  |  t(5)  |  t(2)  |  t(3)  |  t(4)  |
 *            VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(5)  |  t(6)  |  t(3)  |  t(4)  |  t(5)  |  t(6)  |  t(3)  |  t(4)  |
 *                     VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(5)  |  t(6)  |  t(7)  |  t(4)  |  t(5)  |  t(6)  |  t(7)  |  t(4)  |
 *                              VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(5)  |  t(6)  |  t(7)  |  t(8)  |  t(5)  |  t(6)  |  t(7)  |  t(8)  |
 *   VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *   |  t(9)  |  t(6)  |  t(7)  |  t(8)  |  t(9)  |  t(6)  |  t(7)  |  t(8)  |
 *            VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV (processed segment)
 *
 *
 *  Example #1: KRDM_NUMBR_OF_SPORT_BUFFERS = 4
 *	==========
 *
 * 				TX_Buffer (int) 																		BUFFER_BEFORE (float)
 * 				===============      																	====================
 *
 * 	 																								KRDM_index_extract_processed_data
 * 	 		 			   																						| 							|
 * 	 		 		 	   																						V-------					V-------
 *																									mic1 |  t   | t+1  | t+2  | t+3  |  t   | t+1  | t+2  | t+3  |
 *	| MIC1 | ... | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 |...| MIC1 | ... | MIC8 |	==> 	mic2 |  t   | t+1  | t+2  | t+3  |  t   | t+1  | t+2  | t+3  |
 *	 <----------------------------- 8 X SPORT_BUFFER_LENGTH_SAMPLES ------------------>     					| ...  |
 *																									mic8 |  t   | t+1  | t+2  | t+3  |  t   | t+1  | t+2  | t+3  |
 * 						   																						-------- 					--------
 *
 *---------------------------------------------
 */
void KRDM_interleave_to_buffer_before(int* RX_buffer)
{
	int 	mic_number;
	int  	sample_number;
	int 	index_RX;
	int 	index_BUFFER_BEFORE;

	float 	float_value;

	//-----------------------------------------------------
	// 		  Pointer to the segment in BUFFER_BEFORE::
	// 	0,SPORT_BUFFER_LENGTH_SAMPLES,	... , KRDM_FFT_BUFFER_LENGTH_SAMPLES - 1
	//-----------------------------------------------------
	KRDM_index_to_insert_new_data 	+= SPORT_BUFFER_LENGTH_SAMPLES;
	KRDM_index_to_insert_new_data 	= KRDM_index_to_insert_new_data * (KRDM_index_to_insert_new_data < KRDM_FFT_BUFFER_LENGTH_SAMPLES);
	//-----------------------------------------------------
	// 		  Parsing
	//-----------------------------------------------------
	for (sample_number = 0; sample_number < SPORT_BUFFER_LENGTH_SAMPLES; sample_number++)
	{
		for (mic_number = 0; mic_number < KRDM_NUMBER_OF_MICS; mic_number++)
		{
			//-----------------------------------------------------
			// 		  Indices for the two buffers
			//-----------------------------------------------------
			index_RX 					= sample_number * KRDM_NUMBER_OF_MICS + mic_number;
			index_BUFFER_BEFORE 		= mic_number * KRDM_FFT_BUFFER_LENGTH_SAMPLES * 2 + sample_number + KRDM_index_to_insert_new_data;
			//-----------------------------------------------------
			// 		  Rearrange buffer
			//-----------------------------------------------------
			float_value  																	= (float)RX_buffer[index_RX]/POW_2_31;
			KRDM_BUFFER_BEFORE[index_BUFFER_BEFORE].re 										= float_value;
			KRDM_BUFFER_BEFORE[index_BUFFER_BEFORE + KRDM_FFT_BUFFER_LENGTH_SAMPLES].re 	= float_value;
		}
	}
}


/*--------------------------------------------
 *  KRDM_buffer_after_to_interleave
 * 	Alon Slapak 5.3.2019
 * 	Interleave the data from the pseudo-circular array of MICS to interleaved SPORT channels.
 *
 *	Input:
 *		int* TX_buffer 		- pointer to the TX_buffer to the USB
 *	Output:
 *  	void
 *	Algorithm:
 * 		1. KRDM_index_extract_processed_data pointer on the BUFFER_AFTER
 * 		2. 2-dim loop on the BUFFER_AFTER
 *
 *
 *	Example #1: KRDM_NUMBR_OF_SPORT_BUFFERS = 4
 *	==========
 *
 * 				BUFFER_AFTER (float) 													TX_Buffer (int)
 * 				==================== 													===============
 *
 * 	 		KRDM_index_extract_processed_data
 * 	 		 			   |
 * 	 		 		 	   V-------
 *		mic1 |  t   | t+1  | t+2  | t+3  |
 *		mic2 |  t   | t+1  | t+2  | t+3  | 		==> 	| MIC1 | ... | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 |...| MIC1 | ... | MIC8 |
 *		... 			   | ...  |						 <----------------------------- 8 X SPORT_BUFFER_LENGTH_SAMPLES ------------------>
 *		mic8 |  t   | t+1  | t+2  | t+3  |
 * 						   --------
 *
 *---------------------------------------------
 */
void KRDM_buffer_after_to_interleave(int* TX_buffer)
{
	int 	mic_number;
	int  	sample_number;
	int 	index_TX;
	//-----------------------------------------------------
	// 		  8 MICS buffers --> Interleaved data to USB
	//-----------------------------------------------------
	for (sample_number = 0; sample_number < SPORT_BUFFER_LENGTH_SAMPLES; sample_number++)
	{
		for (mic_number = 0; mic_number < KRDM_NUMBER_OF_MICS; mic_number++)
		{
			index_TX 				= sample_number * KRDM_NUMBER_OF_MICS + mic_number;
			TX_buffer[index_TX] 	= (int)(KRDM_BUFFER_AFTER[KRDM_index_extract_processed_data + index_TX] * POW_2_31);
		}
	}
	//-----------------------------------------------------
	// 		  Pointer to the segment in BUFFER_AFTER:
	// 	0, SPORT_BUFFER_LENGTH_SAMPLES,	... , KRDM_NONOVERLAP_LENGTH_SAMPLES - 1
	//-----------------------------------------------------
	KRDM_index_extract_processed_data 	+= SPORT_BUFFER_LENGTH_SAMPLES;
}

/*--------------------------------------------
 *  KRDM_overlap_add
 * 	Alon Slapak 11.3.2019
 * 	Compute the weighted overlap add (WOLA).
 * 	Reference:
 * 		https://ccrma.stanford.edu/~jos/sasp/Weighted_Overlap_Add.html
 *	Input:
 *		int mic_number 					- mic number
 *		complex_float *p_IFFT_buffer 	- pointer to the IFFT result (single mic)
 *	Output:
 *  	void
 *
 * 	Algorithm weighted overlap add (WOLA):
 * 	=========
 * 	 Start: Given the result from the IFFT and the buffer after
 * 		1. Weight:	 	each element in the IFFT result with "synthesis window" (a.k.a,"postwindow" or simply "output window"). x' <-- x .* weight
 * 		2. Overlap add: Add the overlaped segments to the beginning of BUFFER_UFTER
 * 		3. Copy: 		copy the rest of the IFFF to the BUFFER_UFTER
 *
 *
 *	Example #1
 *	==========
 *	KRDM_NUMBR_OF_SPORT_BUFFERS 	= 4
 *	KRDM_NUMBR_OF_OVERLAP_BUFFERS 	= 1
 *
 * 	| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | ...
 * 	 vvvvvvvvvvvvvvv 		 vvvvvvvvvvvvvvvv
 * 	  	         vvvvvvvvvvvvvvv         vvvvvvvvvvvvvvvvvvv
 *   Stage 1:
 *   	IFFT:				| 4  | 5  | 6  | 7  |
 *   	KRDM_BUFFER_AFTER : | 1* | 2* | 3* | 4* |
 *   Stage 1:
 *     	IFFT:				| 4' | 5' | 6' | 7' |
 *   Stage 2:
 *    	KRDM_BUFFER_AFTER : | 4* = 4* + 4' | ------- | ------- | ------- |
 *   Stage 3:
 *    	KRDM_BUFFER_AFTER : | 4* = 4* + 4' | 5* = 5' | 6* = 6' | 7* = 7' |
 *
 *	Example #2
 *	==========
 *	KRDM_NUMBR_OF_SPORT_BUFFERS 	= 4
 *	KRDM_NUMBR_OF_OVERLAP_BUFFERS 	= 2
 *
 * 	| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | ...
 * 	 vvvvvvvvvvvvvvv vvvvvvvvvvvvvvv vvvvvvvvvvvvvvvvvv
 * 	  	     vvvvvvvvvvvvvvv vvvvvvvvvvvvvvvv vvvvvvvvvvvvvvvvvvv
 *   Start:
 *   	IFFT:				| 3  | 4  | 5  | 6  |
 *   	KRDM_BUFFER_AFTER : | 1* | 2* | 3* | 4* |
 *   Stage 1:
 *     	IFFT:				| 3' | 4' | 5' | 6' |
 *   Stage 2:
 *    	KRDM_BUFFER_AFTER : | 3* = 3* + 3' | 4* = 4* + 4' | ------- | ------- |
 *   Stage 3:
 *    	KRDM_BUFFER_AFTER : | 3* = 3* + 3' | 4* = 4* + 4' | 5* = 5' | 6* = 6' |
 *   ...
 *   Next stage:
 *    	KRDM_BUFFER_AFTER : | 5* = 5* + 5' | 6* = 6* + 6' | 7* = 7' | 8* = 8' |
 *---------------------------------------------
 */
void KRDM_overlap_add(int mic_number, complex_float *p_IFFT_buffer)
{
	int 	k;
	int 	TX_index_start 		= mic_number * KRDM_FFT_LENGTH_SAMPLES;
	int 	TX_index_overlap 	= mic_number * KRDM_FFT_LENGTH_SAMPLES + KRDM_NONOVERLAP_LENGTH_SAMPLES;
	//---------------------------------------
	// 		Stage 1: weight
	//---------------------------------------
	for (k = 0 ; k < KRDM_FFT_LENGTH_SAMPLES; k++)
	{
		//TODO : as 11.3.2019 add post windoe for overlap add
		p_IFFT_buffer[k].re 					= p_IFFT_buffer[k].re * 1.0;
	}
	//---------------------------------------
	// 		Stage 2: Overlap add
	//---------------------------------------
	for (k = 0 ; k < KRDM_OVERLAP_LENGTH_SAMPLES; k++)
	{
		KRDM_BUFFER_AFTER[TX_index_start + k] 	=  KRDM_BUFFER_AFTER[TX_index_overlap + k] + p_IFFT_buffer[k].re;
	}
	//---------------------------------------
	// 		Stage 3: Copy
	//---------------------------------------
	for (k = KRDM_OVERLAP_LENGTH_SAMPLES; k < KRDM_FFT_LENGTH_SAMPLES; k++)
	{
		KRDM_BUFFER_AFTER[TX_index_start + k] 	= p_IFFT_buffer[k].re;
	}
}





#endif /* KRDM_SEPARATOR_C_ */
