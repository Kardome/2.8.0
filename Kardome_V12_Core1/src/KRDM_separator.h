/*
 * KRDM_separator.h
 *
 *  Created on: Mar 6, 2019
 *      Author: Kardome
 */

#ifndef KRDM_SEPARATOR_H_
#define KRDM_SEPARATOR_H_

#include <adi_fft_wrapper.h>

#include "../../Kardome_V12_Core0/src/GVL_parameters.h"

//----------------------------------------
// 			Constants
//----------------------------------------
#define KRDM_COMPLEX_FLOAT_BYTES 					8
#define KRDM_FLOAT_BYTES 							4

#define KRDM_NUMBER_OF_MICS 						8
#define POW_2_31 									2147483648.0
/*-------------------------------------------------------
 *	 		Algorithm parameters
 *
 * BUFFER BEFORE (CYCLIC):
 * ==============
 *
 *	  <-------   			KRDM_FFT_BUFFER_LENGTH_SAMPLES 	    ------------>
 *	  <------- 			KRDM_NUMBR_OF_SPORT_BUFFERS = 4		 	------------>
 *									  -->  KRDM_NUMBR_OF_OVERLAP_BUFFERS = 2  <--
 *
 *	 |  RX_BUFFER(t1)  |  RX_BUFFER(t2)  |  RX_BUFFER(t3)  |  RX_BUFFER(t4)  |
 *	  									 |  RX_BUFFER(t3)  |  RX_BUFFER(t4)  |  RX_BUFFER(t5)  |  RX_BUFFER(t6)  |
 *
 *
 * BUFFER AFTER:
 * ==============
 * 	|  buffer(t)  |  buffer(t+1)  |  buffer(t+2)  |  buffer(t+3)  |
 *
 *	 |  			buffer(t)  		   |
 *	  <- SPORT_BUFFER_LENGTH_SAMPLES ->
 *-------------------------------------------------------
 */
#define KRDM_NUMBR_OF_SPORT_BUFFERS 				4
#define KRDM_NUMBR_OF_OVERLAP_BUFFERS 				1
#define KRDM_NUMBR_OF_NONOVERLAP_BUFFERS 			KRDM_NUMBR_OF_SPORT_BUFFERS - KRDM_NUMBR_OF_OVERLAP_BUFFERS

#define KRDM_OVERLAP_LENGTH_SAMPLES 				KRDM_NUMBR_OF_OVERLAP_BUFFERS 	* SPORT_BUFFER_LENGTH_SAMPLES
#define KRDM_FFT_BUFFER_LENGTH_SAMPLES				KRDM_NUMBR_OF_SPORT_BUFFERS 	* SPORT_BUFFER_LENGTH_SAMPLES
#define KRDM_NONOVERLAP_LENGTH_SAMPLES				KRDM_NUMBR_OF_NONOVERLAP_BUFFERS * SPORT_BUFFER_LENGTH_SAMPLES
#define KRDM_FFT_LENGTH_SAMPLES 					KRDM_NUMBR_OF_SPORT_BUFFERS 	* SPORT_BUFFER_LENGTH_SAMPLES

/*----------------------------------------
 *
 *			 Arrays
 *			========
 *	RX_buffer (int, interleaved) 	| MIC1 | MIC2 | MIC3 | MIC4 | MIC5 | MIC6 | MIC7 | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 | MIC1 | ...
 *
 *	TX_buffer (int, interleaved)	| MIC1 | MIC2 | MIC3 | MIC4 | MIC5 | MIC6 | MIC7 | MIC8 | MIC1 | ... | MIC8| MIC1 | ... | MIC8 | MIC1 | ...
 *
 *	BUFFER_BEFORE (complex-float) 	mic1 |  t+2 | t+3  | t    | t+1  |  t+2 | t+3  | t    | t+1  |
 *									mic2 |  t+2 | t+3  | t    | t+1  |  t+2 | t+3  | t    | t+1  |
 *									...
 *									mic8 |  t+2 | t+3  | t    | t+1  |  t+2 | t+3  | t    | t+1  |
 *
 *	BUFFER_AFTER (float)			mic1 |  t   | t+1  | t+2  | t+3  |
 *									mic2 |  t   | t+1  | t+2  | t+3  |
 *									...
 *									mic8 |  t   | t+1  | t+2  | t+3  |
 *
 * 	BUFFER_FFT (complex-float) 		|  t   | t+1  | t+2  | t+3  |
 *
 * 	BUFFER_IFFT (complex-float) 	|  t   | t+1  | t+2  | t+3  |
 *
 *----------------------------------------
 */
#define KRDM_BUFFER_BEFORE_LENGTH_BYTES 			KRDM_NUMBER_OF_MICS * KRDM_FFT_BUFFER_LENGTH_SAMPLES * KRDM_COMPLEX_FLOAT_BYTES * 2   // X2 for pseudo-circular-array (PCA)
#define KRDM_BUFFER_AFTER_LENGTH_BYTES 				KRDM_NUMBER_OF_MICS * KRDM_FFT_BUFFER_LENGTH_SAMPLES * KRDM_FLOAT_BYTES

//---------------------------------------------
// 			SHARC0 buffers
//---------------------------------------------
#define GVL_SHARC0_DATA2_ADDRESS 					0x81000000u 	/*{ 0x81000000u, 0x88FFFFFFu, SHARC_L3}, 128MB DDR-A : SHARC0 data2*/

#define KRDM_BUFFER_BEFORE_ADDRESS 					GVL_SHARC0_DATA2_ADDRESS
#define KRDM_BUFFER_AFTER_ADDRESS 	 				GVL_SHARC0_DATA2_ADDRESS 	+ KRDM_BUFFER_BEFORE_LENGTH_BYTES
#define KRDM_BUFFER_FFT_ADDRESS 					KRDM_BUFFER_AFTER_ADDRESS 	+ KRDM_BUFFER_AFTER_LENGTH_BYTES
#define KRDM_BUFFER_IFFT_ADDRESS 					KRDM_BUFFER_FFT_ADDRESS 	+ KRDM_FFT_BUFFER_LENGTH_SAMPLES * KRDM_COMPLEX_FLOAT_BYTES


//#define KRDM_BUFFER_AFTER_ADDRESS 	 				GVL_SHARC0_DATA2_ADDRESS + KRDM_BUFFER_BEFORE_LENGTH_BYTES


//----------------------------------------
// 			Functions
//----------------------------------------
void 	KRDM_init();
void 	KRDM_operate(int* RX_buffer, int* TX_buffer);
void 	KRDM_interleave_to_buffer_before(int* RX_buffer);
void 	KRDM_buffer_after_to_interleave(int* TX_buffer);
void 	KRDM_overlap_add(int mic_number, complex_float *p_IFFT_buffer);


#endif /* KRDM_SEPARATOR_H_ */
