/*
 * LEDs_and_BUTTONs.c
 *
 *  Created on: May 29, 2018
 *      Author: Kardome
 */


#include "LEDs_and_BUTTONs.h"

//static void 	PUSH_BUTTON_callback(ADI_GPIO_PIN_INTERRUPT ePinInt, uint32_t PinIntData,  void *pCBParam);


/*--------------------------------------------
 *  LED_Init
 * 	Alon Slapak 26.7.2018
 * 	Initializes the LEDs .
 *	Input:
 *		void
 * 	Output:
 *  	ADI_GPIO_RESULT:
 *  		ADI_GPIO_SUCCESS
 *			ADI_GPIO_FAILURE.
 *
 * --------------------------------------------
 */
ADI_GPIO_RESULT LED_Init(void)
{
	ADI_GPIO_RESULT 	GPIO_result;
	GPIO_result = adi_gpio_SetDirection(LED_1_PORT, LED_1_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= LED_1_off();
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	GPIO_result = adi_gpio_SetDirection(LED_2_PORT, LED_2_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= LED_2_off();
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	GPIO_result = adi_gpio_SetDirection(LED_3_PORT, LED_3_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= LED_3_off();
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	GPIO_result = adi_gpio_SetDirection(LED_4_PORT, LED_4_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= LED_4_off();
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	return ADI_GPIO_SUCCESS;
}

/*
 * ------------------------------------------
 *  	LEDs  ON/OFF/Toggle
 * ------------------------------------------
 */
ADI_GPIO_RESULT LED_1_on(void)
{
	return adi_gpio_Clear(LED_1_PORT, LED_1_PIN);
}

ADI_GPIO_RESULT LED_2_on(void)
{
	return adi_gpio_Clear(LED_2_PORT, LED_2_PIN);
}

ADI_GPIO_RESULT LED_3_on(void)
{
	return adi_gpio_Clear(LED_3_PORT, LED_3_PIN);
}
ADI_GPIO_RESULT LED_4_on(void)
{
	return adi_gpio_Clear(LED_4_PORT, LED_4_PIN);
}

ADI_GPIO_RESULT LED_1_off(void)
{
	return adi_gpio_Set(LED_1_PORT, LED_1_PIN);
}

ADI_GPIO_RESULT LED_2_off(void)
{
	return adi_gpio_Set(LED_2_PORT, LED_2_PIN);
}

ADI_GPIO_RESULT LED_3_off(void)
{
	return adi_gpio_Set(LED_3_PORT, LED_3_PIN);
}

ADI_GPIO_RESULT LED_4_off(void)
{
	return adi_gpio_Set(LED_4_PORT, LED_4_PIN);
}

ADI_GPIO_RESULT LED_1_toggle(void)
{
	return adi_gpio_Toggle(LED_1_PORT, LED_1_PIN);
}

ADI_GPIO_RESULT LED_2_toggle(void)
{
	return adi_gpio_Toggle(LED_2_PORT, LED_2_PIN);
}

ADI_GPIO_RESULT LED_3_toggle(void)
{
	return adi_gpio_Toggle(LED_3_PORT, LED_3_PIN);
}

ADI_GPIO_RESULT LED_4_toggle(void)
{
	return adi_gpio_Toggle(LED_4_PORT, LED_4_PIN);
}
/*--------------------------------------------
 *  GPIO_debug_toggle
 * 	Alon Slapak 30.1.2019
 * 	toggle the GPIO C.11. This is a GPIo which is used for debug
 *	Input:
 *		void
 * 	Output:
 *  	ADI_GPIO_RESULT:
 *  		ADI_GPIO_SUCCESS
 *			ADI_GPIO_FAILURE.
 *
 * --------------------------------------------
 */
ADI_GPIO_RESULT GPIO_debug_toggle(void)
{
	ADI_GPIO_RESULT 	GPIO_result;
	GPIO_result 	 = adi_gpio_SetDirection(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result 	|= adi_gpio_Toggle(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	return ADI_GPIO_SUCCESS;
}


/*--------------------------------------------
 *  BUTTON_init
 * 	Alon Slapak 27.1.2019
 * Initialize the Push Button
 *	Input:
 *		ADI_GPIO_CALLBACK p_BUTTON_callback - pointer to the function
 *		to execute when button is pressed
 * 	Output:
 *  	ADI_GPIO_RESULT:
 *  		ADI_GPIO_SUCCESS
 *			ADI_GPIO_FAILURE.
 *
 * --------------------------------------------
 */
ADI_GPIO_RESULT BUTTON_init(ADI_GPIO_CALLBACK p_BUTTON_callback)
{
	ADI_GPIO_RESULT 	GPIO_result;
	static uint8_t 		gpioMemory[ADI_GPIO_CALLBACK_MEM_SIZE];
	uint32_t 			gpioMaxCallbacks;

	/* Initialize the GPIO service */
	GPIO_result = adi_gpio_Init((void*)gpioMemory, GPIO_MEMORY_SIZE, &gpioMaxCallbacks);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	/* Direction  */
	GPIO_result = adi_gpio_SetDirection(PUSH_BUTTON_PORT, PUSH_BUTTON_PIN, ADI_GPIO_DIRECTION_INPUT);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	/* Set edge sense mode  */
	GPIO_result = adi_gpio_SetPinIntEdgeSense(PUSH_BUTTON_PINT, PUSH_BUTTON_PIN, ADI_GPIO_SENSE_FALLING_EDGE);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	/* Register pinint callback */
	GPIO_result = adi_gpio_RegisterCallback(PUSH_BUTTON_PINT, PUSH_BUTTON_PIN, p_BUTTON_callback, (void*)0);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	/* Assign pin interrupt  */
	GPIO_result = adi_gpio_PinInterruptAssignment(PUSH_BUTTON_PINT, PUSH_BUTTON_ASSIGN_BYTE, PUSH_BUTTON_ASSIGN);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	/* Set pin interrupt mask */
	GPIO_result = adi_gpio_EnablePinInterruptMask(PUSH_BUTTON_PINT, PUSH_BUTTON_PIN, true);
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GPIO_result;
	}
	return ADI_GPIO_SUCCESS;
}
