/*==============================================================================
    FILE:           GVL.c

    DESCRIPTION:    Provides a interface between the user_audio USB Audio 2.0
                    functionality and the ADI adau1979 and adau1962a drivers.

    Copyright (c) 2018 Closed Loop Design, LLC

    This software is supplied "AS IS" without any warranties, express, implied
    or statutory, including but not limited to the implied warranties of fitness
    for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
    extends you a royalty-free right to use, reproduce and distribute this source
    file as well as executable files created using this source file for use with 
    Analog Devices SC5xx family processors only. Nothing else gives you 
    the right to use this source file.

==============================================================================*/
/*!
 * @file      GVL.c
 * @brief     Interface logic for the ADAU1979 and ADAU1962a drivers.
 *
 * @details
 *            Provides an interface between the user_audio USB Audio 2.0
 *            functionality and the ADI adau1979 and adau1962a drivers.
 *
 *            Copyright (c) 2018 Closed Loop Design, LLC
 *
 *            This software is supplied "AS IS" without any warranties, express, implied
 *            or statutory, including but not limited to the implied warranties of fitness
 *            for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
 *            extends you a royalty-free right to use, reproduce and distribute this source
 *            file as well as executable files created using this source file for use with 
 *            Analog Devices SC5xx family processors only. Nothing else gives you 
 *            the right to use this source file.
 *
 */

#include "GVL.h"
#include "sruSC589.h"
/*---------------------------------------------------
 * 		Static functions
 *----------------------------------------------------*/
static void 			GVL_rx_callback(void *pCBParam, uint32_t Event, void *pArg);
static GVL_RESULT 		GVL_submit_buffers_to_SPORT(void);
static void 			GVL_SRU_Init(void);

/*---------------------------------------------------
 * 		Buffer
 *----------------------------------------------------*/
int*	 				p_GVL_RX_from_SPORT_PING  	= (int*)ADDRESS_RX_from_SPORT_PING;
int*					p_GVL_RX_from_SPORT_PONG 	= (int*)ADDRESS_RX_from_SPORT_PONG;
int* 					p_GVL_TX_to_USB_PING 		= (int*)ADDRESS_TX_to_USB_PING;
int* 					p_GVL_TX_to_USB_PONG 		= (int*)ADDRESS_TX_to_USB_PONG;

int 					AMIR[256*8];

static int 				GVL_Rx_pingpong_index;
/*---------------------------------------------------
 * 		Static variables
 *----------------------------------------------------*/
unsigned int 			GVL_SPORT_buffer_length_bytes;
T_GVL_configuration 	GVL_configuration;

//static unsigned int 	GVL_decimation_factor;

#define min(a, b) ((a) < (b)) ? (a) : (b)

/*--------------------------------------------
 *  GVL_init
 * 	Alon Slapak 27.12.2018
 * 	Initialize:
 * 		1. USB
 * 		2. LEDs
 * 		3. CLOCKS
 * 		4. linked list GVL_ll_Rx
 * 		5. SPORT
 *	NOTE: the GVL_init might be read several times until succeeding as the USB_init()
 *	may not pass at the first time.
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_adau1979_1962a_init.c
 *		Function:	user_adau1979_1962a_main
 *	Input:
 *		T_GVL_initialization* GVL_initialization - Structure of initialization parameters.
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
GVL_RESULT GVL_init(T_GVL_configuration* p_GVL_configuration)
{
	ADI_SPORT_RESULT 		SPORT_result;
	ADI_TMR_RESULT			TMR_result;
	ADI_PWR_RESULT			PWR_result;
	USB_RESULT 				USB_result = USB_INIT_ONGOING;

//	int k;
//	for (k = 0; k < 2048; k++)
//	{
//		AMIR[k] = 0;// (k-512) << 18;
//	}

	GVL_configuration.FLAG_loopback 					= p_GVL_configuration->FLAG_loopback;
	GVL_configuration.SPORT_buffer_length_samples		= p_GVL_configuration->SPORT_buffer_length_samples;
	GVL_configuration.Sampling_frequency_Hz 			= p_GVL_configuration->Sampling_frequency_Hz;
	GVL_configuration.p_BUTTON_callback 				= p_GVL_configuration->p_BUTTON_callback;
	/*----------------------------------------------
	 * 	Initialize the Initialize the Signal Routing Unit (SRU)
	 *----------------------------------------------*/
	GVL_SRU_Init();
	/*----------------------------------------------
	 *  Initializes ADI power system service, Core clock and system clock.
	 * 	Core clock is 442.368 MHz and system clock is 221.184 MHz, so that
	 * 	the SPORT can produce accurate BCLK of 6.144 MHz. Therefore, the clock
	 * 	is extracted from a 24.576 MHz xtal.
	 *----------------------------------------------*/
	if ((PWR_result = CLOCKS_power_and_system_clock_init()) != ADI_PWR_SUCCESS)
	{
		return GVL_INIT_POWER_AND_SYSTEM_CLOCK_FAILED;
	}
	/*----------------------------------------------
	 * 	Initializes the timer of the USB to call the cld_time_125us_tick.c
	 * 	using interrupt every 125 uSec, that is 8000 Hz.
	 *----------------------------------------------*/
	if ((TMR_result = CLOCKS_timer_init(USB_timer_handler)) != ADI_TMR_SUCCESS)
	{
		return GVL_INIT_TIMER_FAILED;
	}
	//----------------------------------------
	// 	Initialize Rx buffer length
	//----------------------------------------
	if (GVL_configuration.FLAG_loopback)
	{
		// (GVL_configuration->Sampling_frequency_Hz / 8000) is the number of samples per USB packet
		// which is one transfer per 125 us or 8,000 transfers per second. Therefore the number of samples
		// per packet is the sampling frequency divided by 8000 * 4 (bytes) * 8 (channels).
		// For example, for 48kHz, the buffer length is 6 * 4 * 8 = 192
		GVL_SPORT_buffer_length_bytes = (GVL_configuration.Sampling_frequency_Hz / 8000) *  GVL_SAMPLE_SIZE_BYTES * GVL_NUM_AUDIO_CHANNELS;
	}
	else
	{
		// The number of bytes is the number of samples * number of channels * 4 bytes per sample. There is
		// a maximum length of the buffer GVL_MAX_BUFFER_SIZE_SAMPLES.
		GVL_SPORT_buffer_length_bytes = min((int)GVL_MAX_BUFFER_SIZE_SAMPLES,(int)GVL_configuration.SPORT_buffer_length_samples) * GVL_SAMPLE_SIZE_BYTES * GVL_NUM_AUDIO_CHANNELS;
	}
	/*----------------------------------------------
	 * 		Initialize USB.
	 * 		The USB_init may be invoked several times before succeeding
	 *----------------------------------------------*/
	T_USB_configuration 	USB_configuration;
	USB_configuration.FLAG_loopback 				= GVL_configuration.FLAG_loopback;
	USB_configuration.Sampling_frequency_Hz 		= GVL_configuration.Sampling_frequency_Hz;
	USB_configuration.SPORT_buffer_length_samples 	= GVL_configuration.SPORT_buffer_length_samples;

	while (USB_INIT_ONGOING == USB_result)
	{
		USB_result 		= USB_init(&USB_configuration);
		if (USB_result == USB_INIT_FAILED)
		{
			return GVL_INIT_USB_FAILED;
		}
	}
	//----------------------------------------
	// 	Initialize LEDS
	//----------------------------------------
	if (LED_Init() != ADI_GPIO_SUCCESS)
	{
		return GVL_INIT_LED_FAILED;
	}
	//----------------------------------------
	// 	Initialize Push button
	//----------------------------------------
	if (GVL_configuration.p_BUTTON_callback != NULL)
	{
		if (BUTTON_init(GVL_configuration.p_BUTTON_callback) != ADI_GPIO_SUCCESS)
		{
			return GVL_INIT_BUTTON_FAILED;
		}
	}
	//----------------------------------------
	// 		Initializes the clocks of the microphones.
	//----------------------------------------
	if (CLOCKS_Init() != ADI_GPIO_SUCCESS)
	{
		return GVL_INIT_CLOCK_BUFFER_FAILED;
	}
	//----------------------------------------------
	// 		Initialize the SPORT Rx
	//----------------------------------------------
	if ((SPORT_result = SPORT_TDM_Init(GVL_configuration.Sampling_frequency_Hz)) != ADI_SPORT_SUCCESS)
	{
		return GVL_SPORT_INIT_FAILED;
	}
	//----------------------------------------------
	// 		Register callback to SPORT Rx
	//----------------------------------------------
	if ((SPORT_result = SPORT_TDM_RegisterCallback(GVL_rx_callback, NULL)) != ADI_SPORT_SUCCESS)
	{
		return GVL_SPORT_REGISTER_CALLBACK_FAILED;
	}
	//--------------------------------------
	// 		Ping-pong index
	//--------------------------------------
	GVL_Rx_pingpong_index = 0;
	//--- --------------------------------------
	//  	 	Submit buffer to SPORT Rx
	//-----------------------------------------
	if (GVL_submit_buffers_to_SPORT() != GVL_SUCCESS)
	{
		return GVL_SPORT_SUBMIT_BUFFERS_FAILED;
	}
	//-----------------------------------------
	//  	Enable SPORT
	//-----------------------------------------
	if (SPORT_TDM_Enable(true) != ADI_SPORT_SUCCESS)
	{
		return GVL_SPORT_ENABLE_FAILED;
	}

	return GVL_SUCCESS;
}

/*--------------------------------------------
 *  GVL_main
 * 	Alon Slapak 27.12.2018
 * 	Main function which is called routinely (while loop), and performed:
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_adau1979_1962a.c
 *		Function:	user_adau1979_1962a_main
 *	Input:
 *		void
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
GVL_RESULT GVL_main (void)
{
	/*----------------------------------------------
	 * LED toggling for health indication
	 *----------------------------------------------*/
	static unsigned long main_time 	= 0;
	if (cld_time_passed_ms(main_time) >= 250)
	{
		main_time = cld_time_get();
		LED_1_toggle();
	}
	/*----------------------------------------------
	 * 	Invokes the CLD library mainline function, which is
	 * 	required and should be called in each iteration of the main program loop.
	 *----------------------------------------------*/
	USB_main();

	return GVL_SUCCESS;
}

/*--------------------------------------------
 *  GVL_rx_callback
 * 	Alon Slapak 25.1.2019
 * 	Function called when SPORT data has been received.
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_adau1979_1962a.c
 *		Function:	user_adau1979_1962a_rx_callback
 *	Input:
 *		void *pCBParam 	- Callback parameters pointer
 *		uint32_t Event 	- Event that triggered this callback function.
 *		void *pArg 		- Pointer to the data received by the Sport.
 * 	Output:
 *  	void
 * --------------------------------------------
 */
static void GVL_rx_callback(void *pCBParam, uint32_t Event, void *pArg)
{
	//--------------------------------------
	// 	Event: Audio input data received.
	//--------------------------------------
	if (Event == (unsigned long)ADI_SPORT_EVENT_RX_BUFFER_PROCESSED)
	{
		//--------------------------------------
		// 	Submit PING to SPORT
		//--------------------------------------
		if (GVL_Rx_pingpong_index == 0)
		{
			GVL_Rx_pingpong_index = 1;

			int k;
			int aaa;
			int counter = 0;
			for (k = 0; k < SPORT_BUFFER_LENGTH_SAMPLES * 8; k += 8)
			{
				counter++;
				aaa = (int)(counter) << 17;
				p_GVL_RX_from_SPORT_PING[k] =  aaa;//1 << 31; //pow(2,31);
			}
			//--------------------------------------
			// 	 Process the audio samples
			//--------------------------------------
			*pREG_SEC0_RAISE = (INTR_SOFT0 - ADI_INTR_BASE);
			//--------------------------------------
			// 	 Send processed buffer to USB
			//--------------------------------------
//			if (!GVL_configuration.FLAG_loopback)
			{
				USB_transmit_data(p_GVL_TX_to_USB_PING, SPORT_BUFFER_LENGTH_SAMPLES * 8); //TODO 25.3.2019 replace constants with varialbe
			}
			//--------------------------------------
			// 	 Re-Submit PING buffer to SPORT
			//--------------------------------------
			if (SPORT_TDM_Submit_buffer(p_GVL_RX_from_SPORT_PING, GVL_SPORT_buffer_length_bytes) != ADI_SPORT_SUCCESS)
			{
				//TODO 27.1.2019: indicate error (LED?)
			}
		}
		//--------------------------------------
		// 	Submit PONG to SPORT
		//--------------------------------------
		else
		{
			GVL_Rx_pingpong_index = 0;

			int k;
			for (k = 0; k < SPORT_BUFFER_LENGTH_SAMPLES * 8; k += 8)
			{
				p_GVL_RX_from_SPORT_PONG[k] =  0;
			}
			//--------------------------------------
			// 	 Process the audio samples
			//--------------------------------------
			*pREG_SEC0_RAISE = (INTR_SOFT1 - ADI_INTR_BASE);
			//--------------------------------------
			// 	 Send processed buffer to USB
			//--------------------------------------
//			if (!GVL_configuration.FLAG_loopback)
			{
				USB_transmit_data(p_GVL_TX_to_USB_PONG, SPORT_BUFFER_LENGTH_SAMPLES * 8);  //TODO 25.3.2019 replace constants with varialbe
			}
			//--------------------------------------
			// 	 Re-Submit PONG buffer to SPORT
			//--------------------------------------
			if (SPORT_TDM_Submit_buffer(p_GVL_RX_from_SPORT_PONG, GVL_SPORT_buffer_length_bytes ) != ADI_SPORT_SUCCESS)
			{
				//TODO 27.1.2019: indicate error (LED?)
			}
		}
	}
	//--------------------------------------
	// 	 Event: other
	//--------------------------------------
	else
	{
		//TODO 27.1.2019: indicate error (LED?)
	}
}

/*--------------------------------------------
 *  GVL_submit_buffers_to_SPORT
 * 	Alon Slapak 27.12.2018
 * 	Submit ping-pong buffers to the SOPRT Rx.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_adau1979_1962a.c
 *		Function:	Adau1979SubmitBuffers
 *	Input:
 *		void
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
static GVL_RESULT GVL_submit_buffers_to_SPORT(void)
{
	//---------------------------------------------
	// 		submit buffer PING
	//---------------------------------------------
	if (SPORT_TDM_Submit_buffer(p_GVL_RX_from_SPORT_PING,GVL_SPORT_buffer_length_bytes) != ADI_SPORT_SUCCESS)
	{
		return GVL_FAILED;
	}
	//---------------------------------------------
	// 		submit buffer PONG
	//---------------------------------------------
	if (SPORT_TDM_Submit_buffer(p_GVL_RX_from_SPORT_PONG,GVL_SPORT_buffer_length_bytes) != ADI_SPORT_SUCCESS)
	{
		return GVL_FAILED;
	}
	return GVL_SUCCESS;
}



/*--------------------------------------------
 *  GVL_LED_on
 * 	Alon Slapak 27.1.2019
 * 	Turns ON a LED
 *	Input:
 *		int led_number - number of LED (1-4)
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
GVL_RESULT GVL_LED_on(uint32_t led_number)
{
	switch (led_number)
	{
	case 1:
		return LED_1_on();
		break;
	case 2:
		return LED_2_on();
		break;
	case 3:
		return LED_3_on();
		break;
	case 4:
		return LED_4_on();
		break;
	default:
		return GVL_FAILED;
	}
}
/*--------------------------------------------
 *  GVL_LED_off
 * 	Alon Slapak 27.1.2019
 * 	Turns OFF a LED
 *	Input:
 *		uint32_t led_number - number of LED (1-4)
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
GVL_RESULT GVL_LED_off(uint32_t led_number)
{
	switch (led_number)
	{
	case 1:
		return LED_1_off();
		break;
	case 2:
		return LED_2_off();
		break;
	case 3:
		return LED_3_off();
		break;
	case 4:
		return LED_4_off();
		break;
	default:
		return GVL_FAILED;
	}
}
/*--------------------------------------------
 *  GVL_LED_toggle
 * 	Alon Slapak 27.1.2019
 * 	Toggle a LED
 *	Input:
 *		uint32_t led_number - number of LED (1-4)
 * 	Output:
 *  	GVL_RESULT
 * --------------------------------------------
 */
GVL_RESULT GVL_LED_toggle(uint32_t led_number)
{
	switch (led_number)
	{
	case 1:
		return LED_1_toggle();
		break;
	case 2:
		return LED_2_toggle();
		break;
	case 3:
		return LED_3_toggle();
		break;
	case 4:
		return LED_4_toggle();
		break;
	default:
		return GVL_FAILED;
	}
}
/*--------------------------------------------
 *  GVL_GPIO_debug_toggle
 * 	Alon Slapak 30.1.2019
 * 	toggle the GPIO C.11. This is a GPIo which is used for debug
 *	Input:
 *		void
 * 	Output:
 *  	GVL_RESULT
 *
 * --------------------------------------------
 */
GVL_RESULT GVL_GPIO_debug_toggle(void)
{
	ADI_GPIO_RESULT	 GPIO_result;
	GPIO_result 	= GPIO_debug_toggle();
	if (GPIO_result != ADI_GPIO_SUCCESS)
	{
		return GVL_FAILED;
	}
	return GVL_SUCCESS;
}
/*--------------------------------------------
 *  GVL_SRU_Init
 * 	Alon Slapak 27.1.2019
 * 	Initialize the Signal Routing Unit (SRU)
 * 	The code is equivalent to using the GUI (Project explorer --> system.svc --> Signal Routing Unit --> Configuration).
 * 	The GUI creates the adi_SRU_Init function in system --> sru --> sru_config.c, abd was copied from there.
 * 	The steps in the GUI are:
 * 	1.	DAI0 Pin Buffer --> DAI0_PBEN01_I:
 * 		1.1.	Remove SPT0A_D0_PBEN_O
 * 		1.2.	Add DAI0_LOW
 * 	2.	DAI0 Pin Buffer --> DAI0_PBEN02_I:
 * 		2.1.	Remove SPT0A_D1_PBEN_O
 * 		2.2.	Add DAI0_LOW
 * 	3.	SPORT 0A  --> SPT0A_CLK_O:
 * 		3.1.	Add SPT0A_CLK_I (Confirm replacing with existing connection of SPT0A_CLK_I)
 *
 *	Input:
 *		void
 * 	Output:
 *  	void
 * --------------------------------------------
 */
void GVL_SRU_Init(void)
{
//	/* SPT2B_CLK_I, SPT0B_CLK_I, SPT1B_CLK_I, SPT1A_CLK_I, SPT0A_CLK_I, SPT2A_CLK_I */
//	*pREG_DAI0_CLK0 = (unsigned int) 0x252630d4;
//
//	/* DAI0_PBEN03_I, DAI0_PBEN02_I, DAI0_PBEN01_I, DAI0_PBEN05_I, DAI0_PBEN04_I */
//	*pREG_DAI0_PBEN0 = (unsigned int) 0x0e248000;
//
//	/* PADS0 DAI0 Port Input Enable Control Register */
//	*pREG_PADS0_DAI0_IE = (unsigned int) 0x001FFFFE;
//
//	/* PADS0 DAI1 Port Input Enable Control Register */
//	*pREG_PADS0_DAI1_IE = (unsigned int) 0x001FFFFE;

	*pREG_PADS0_DAI0_IE = 0x001FFFFE; 		/* PADS0 DAI0 Port Input Enable Control Register */
	SRU(SPT0_ACLK_O, DAI0_PB03_I);
	SRU(SPT0_ACLK_PBEN_O, DAI0_PBEN03_I);
	SRU(SPT0_ACLK_O, SPT0_ACLK_I);

	SRU(SPT0_AFS_O, DAI0_PB04_I);
	SRU(SPT0_AFS_PBEN_O, DAI0_PBEN04_I);

	SRU(DAI0_PB01_O,SPT0_AD0_I);
	SRU(DAI0_PB02_O,SPT0_AD1_I);
}
