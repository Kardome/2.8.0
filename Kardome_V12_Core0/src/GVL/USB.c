/*==============================================================================
    FILE:           USB.c

    DESCRIPTION:    Uses the cld_sc58x_audio_2_0_lib library to implement a basic
                    USB audio 2.0 device.

    Copyright (c) 2018 Closed Loop Design, LLC

    This software is supplied "AS IS" without any warranties, express, implied
    or statutory, including but not limited to the implied warranties of fitness
    for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
    extends you a royalty-free right to use, reproduce and distribute this source
    file as well as executable files created using this source file for use with 
    Analog Devices SC5xx family processors only. Nothing else gives you 
    the right to use this source file.

==============================================================================*/
/*!
 * @file      user_audio_w_cdc.c
 * @brief     Uses the cld_sc58x_audio_2_0_lib library to implement a basic
 *            USB Audio 2.0 device.
 *
 * @details
 *            User defined interface with the cld_sc58x_audio_2_0_lib library to
 *            implement a custom USB Audio 2.0 device.
 *
 *            Copyright (c) 2018 Closed Loop Design, LLC
 *
 *            This software is supplied "AS IS" without any warranties, express, implied
 *            or statutory, including but not limited to the implied warranties of fitness
 *            for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
 *            extends you a royalty-free right to use, reproduce and distribute this source
 *            file as well as executable files created using this source file for use with 
 *            Analog Devices SC5xx family processors only. Nothing else gives you 
 *            the right to use this source file.
 *
 */

#include "USB.h"
#include "GVL.h"


#pragma pack (1)

/*
 * ---------------------------------------------------------------------------
 *    USB Audio v2.0 Unit and Terminal descriptors that describe a simple
 *       audio device comprised of the following:
 *
 *    Input Terminal - USB Streaming Endpoint
 *         ID = 0x01
 *         Channels: Left, Right
 *   Input Terminal - Microphone
 *         ID = 0x02
 *         Channels: Left, Right
 *   Output Terminal - Speaker
 *         ID = 0x06
 *         Source ID = 0x09
 *  Output Terminal - USB Streaming Endpoint
 *         ID = 0x07
 *         Source ID = 0x0a
 *  Feature Unit
 *         ID = 0x09
 *         Source ID = 0x01
 *  Controls:
 *         Master Channel 0: 	Mute (Control 1)
 *         Channel 1 (Left): 	Volume (Control 2)
 *         Channel 2 (Right): 	Volume (Control 2)
 *  Feature Unit
 *         ID = 0x0a
 *         Source ID = 0x02
 *  Controls:
 *         Master Channel 0: Volume (Control 2)
 * --------------------------------------------------------------------------------
 */
static const unsigned char USB_unit_and_terminal_descriptor[] =  /*!< USB Audio v2.0 Unit and Terminal descriptors that describe a simple audio device. */
{
		//----------------------------------------
		// 		Input Terminal Descriptor - USB Endpoint
		//----------------------------------------
		0x11,                   /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x02,                   /* bDescriptorSubType = Input Terminal */
		0x01,                   /* bTerminalID */
		0x01, 0x01,             /* wTerminalType = USB Streaming */
		0x00,                   /* bAssocTerminal */
		0x03,                   /* bCSourceID */
		USB_NUM_AUDIO_CHANNELS,                   /* bNRChannels */

		/* wChannelConfig */
		((1 << USB_NUM_AUDIO_CHANNELS)-1) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 8) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 16) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 24) & 0xff,

		0x00,                   /* iChannelNames */
		0x00,0x00,              /* bmControls */
		0x00,                   /* iTerminal */
		//----------------------------------------
		// 		Input Terminal Descriptor - Microphone
		//----------------------------------------
		0x11,                   /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x02,                   /* bDescriptorSubType = Input Terminal */
		0x02,                   /* bTerminalID */
		0x01, 0x02,             /* wTerminalType = Microphone */
		0x00,                   /* bAssocTerminal */
		//    0x04,                   /* bCSourceID */ //XXX
		0x03,                   /* bCSourceID */ //XXX
		USB_NUM_AUDIO_CHANNELS,                   /* bNRChannels */

		/* wChannelConfig */
		((1 << USB_NUM_AUDIO_CHANNELS)-1) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 8) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 16) & 0xff,
		(((1 << USB_NUM_AUDIO_CHANNELS)-1) >> 24) & 0xff,

		0x00,                   /* iChannelNames */
		0x00,0x00,              /* bmControls */
		0x00,                   /* iTerminal */
		//----------------------------------------
		// 		Output Terminal Descriptor - Speaker
		//----------------------------------------
		0x0c,                   /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x03,                   /* bDescriptorSubType = Output Terminal */
		0x06,                   /* bTerminalID */
		0x01, 0x03,             /* wTerminalType - Speaker */
		0x00,                   /* bAssocTerminal */
		0x09,                   /* bSourceID */
		0x03,                   /* bCSourceID */
		0x00, 0x00,             /* bmControls */
		0x00,                   /* iTerminal */
		//----------------------------------------
		// 		Output Terminal Descriptor - USB Endpoint
		//----------------------------------------
		0x0c,                   /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x03,                   /* bDescriptorSubType = Output Terminal */
		0x07,                   /* bTerminalID */
		0x01, 0x01,             /* wTerminalType - USB Streaming */
		0x00,                   /* bAssocTerminal */
		0x0a,                   /* bSourceID */
		//    0x04,                   /* bCSourceID */ //XXX
		0x03,                   /* bCSourceID */
		0x00, 0x00,             /* bmControls */
		0x00,                   /* iTerminal */
		//----------------------------------------
		//			Feature Unit Descriptor
		//----------------------------------------
		//0x2a,                   /* bLength */
		(10 + (4 * USB_NUM_AUDIO_CHANNELS)), /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x06,                   /* bDescriptorSubType = Feature Unit */
		0x09,                   /* bUnitID */
		0x01,                   /* bSourceID */
		0x00, 0x00, 0x00, 0x00, /* bmaControls - Master */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Left */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Right */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Center */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - LFE */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Back Left */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Back Right */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - FLC */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - FRC */
		0x00,                   /* iFeature */
		//----------------------------------------
		// 			Feature Unit Descriptor
		//----------------------------------------
		(10 + (4 * USB_NUM_AUDIO_CHANNELS)), /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x06,                   /* bDescriptorSubType = Feature Unit */
		0x0A,                   /* bUnitID */
		0x02,                   /* bSourceID */
		0x00, 0x00, 0x00, 0x00, /* bmaControls - Master */

		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Left */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Right */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Front Center */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - LFE */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Back Left */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - Back Right */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - FLC */
		0x0d, 0x00, 0x00, 0x00, /* bmaControls - FRC */

		0x00,                   /* iFeature */
		//----------------------------------------
		// 		Clock Source Descriptor
		//----------------------------------------
		0x08,                   /* bLength */
		0x24,                   /* bDescriptorType = Class Specific Interface */
		0x0a,                   /* bDescriptorSubType = Clock Source */
		0x03,                   /* ClockID */
		//    0x01,                   /* bmAttributes - Internal Fixed Clock */ //XXX
		//    0x00,                   /* bmControls */	//XXX
		//    0x00,                   /* bAssocTerminal */ 	//XXX
		//    0x00,                   /* iClockSource */ 	//XXX
		0x03,                   /* bmAttributes - Internal Fixed Clock */
		0x07,                   /* bmControls */
		0x00,                   /* bAssocTerminal */
		0x09,                   /* iClockSource */
		/* Clock Source Descriptor */ 	//XXX
		//    0x08,                   /* bLength */
		//    0x24,                   /* bDescriptorType = Class Specific Interface */
		//    0x0a,                   /* bDescriptorSubType = Clock Source */
		//    0x04,                   /* ClockID */
		//    0x01,                   /* bmAttributes - Internal Fixed Clock */
		//    0x00,                   /* bmControls */
		//    0x00,                   /* bAssocTerminal */
		//    0x00,                   /* iClockSource */
};

/*
 * ----------------------------------------------------------------
 * 			Isochronous IN endpoint PCM format descriptor
 * ----------------------------------------------------------------
 */
static const unsigned char USB_in_stream_format_descriptor[] =
{
		0x06,               /* bLength */
		0x24,               /* bDescriptorType - Class Specific Interface */
		0x02,               /* bDescriptorSubType - Format Type */
		0x01,               /* bFormatType - Format Type 1 */
		0x04,               /* bSubSlotSize */
		USB_BIT_RESOLUTION, /* bBitResolution */
};

/*
 * ----------------------------------------------------------------
 * 			Isochronous OUT endpoint PCM format descriptor
 * ----------------------------------------------------------------
 */
static const unsigned char USB_out_stream_format_descriptor[] =
{
		0x06,               /* bLength */
		0x24,               /* bDescriptorType - Class Specific Interface */
		0x02,               /* bDescriptorSubType - Format Type */
		0x01,               /* bFormatType - Format Type 1 */
		0x04,               /* bSubSlotSize */
		USB_BIT_RESOLUTION, /* bBitResolution */
};


#pragma pack ()


/*
 * ----------------------------------------------------------------
 * 		IN Audio Stream Interface Endpoint Data Descriptor
 * ---------------------------------------------------------------
 */
static const CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor USB_in_stream_endpoint_desc =
{
		.b_length = sizeof(CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor),
		.b_descriptor_type                  = 0x25,             /* Class Specific Endpoint */
		.b_descriptor_subtype               = 0x01,             /* Endpoint - General */
		.bm_attributes                      = 0x00,             /* max packet only set to 0 */
		.bm_controls                        = 0x00,
		.b_lock_delay_units                 = 0x00,             /* Undefined */
		.w_lock_delay                       = 0x00,
};

/*
 * ----------------------------------------------------------------
 * 		OUT Audio Stream Interface Endpoint Data Descriptor
 * ---------------------------------------------------------------
 */
static const CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor USB_out_stream_endpoint_desc =
{
		.b_length = sizeof(CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor),
		.b_descriptor_type                  = 0x25,             /* Class Specific Endpoint */
		.b_descriptor_subtype               = 0x01,             /* Endpoint - General */
		.bm_attributes                      = 0x00,             /* max packet only set to 0 */
		.bm_controls                        = 0x00,
		.b_lock_delay_units                 = 0x02,             /* Milliseconds */
		.w_lock_delay                       = 0x01,             /* 1 Millisecond */
};


/*
 * ----------------------------------------------------------------
 * 		Function
 * ---------------------------------------------------------------
 */
static void 									USB_data_transmit_completed (void);
static void 									USB_data_transmit_aborted (void);

static CLD_USB_Transfer_Request_Return_Type 	USB_data_received (CLD_USB_Transfer_Params* p_transfer_data);
static CLD_USB_Data_Received_Return_Type 		USB_data_receive_completed (void);

static CLD_USB_Transfer_Request_Return_Type 	user_audio_set_req_cmd (CLD_SC58x_Audio_2_0_Cmd_Req_Parameters* p_req_params, CLD_USB_Transfer_Params* p_transfer_data);
static CLD_USB_Transfer_Request_Return_Type 	user_audio_get_req_cmd (CLD_SC58x_Audio_2_0_Cmd_Req_Parameters* p_req_params, CLD_USB_Transfer_Params* p_transfer_data);
static CLD_USB_Data_Received_Return_Type 		user_audio_set_volume_req (void);
static void 									user_audio_streaming_rx_endpoint_enabled (CLD_Boolean enabled);
static void 									user_audio_streaming_tx_endpoint_enabled (CLD_Boolean enabled);

static void 									USB_event (CLD_USB_Event event);
static void 									USB_tx_audio_data (void);
static void 									user_audio_tx_feedback_data (void);
static void 									user_audio_feedback_xfr_done (void);

static void 									user_audio_usb0_isr(uint32_t Event, void *pArg);

static void 									user_cld_lib_status (unsigned short status_code, void * p_additional_data, unsigned short additional_data_size);

/*
 * ----------------------------------------------------------------
 * 		Audio Stream IN Interface parameters
 * ---------------------------------------------------------------
 */
static CLD_SC58x_Audio_2_0_Stream_Interface_Params USB_in_endpoint_params =
{
		.endpoint_number            = 2,                        /* Isochronous endpoint number */
		.max_packet_size_full_speed = USER_AUDIO_MAX_PACKET_SIZE,/* Isochronous endpoint full-speed max packet size */
		.max_packet_size_high_speed = USER_AUDIO_MAX_PACKET_SIZE,/* Isochronous endpoint high-speed max packet size */
		.b_interval_full_speed      = 1,                        /* Isochronous endpoint full-speed bInterval */
		.b_interval_high_speed      = 2,                        /* Isochronous endpoint high-speed bInterval */
		.b_terminal_link            = 7,                        /* Terminal ID of the associated Output Terminal */
		.b_format_type              = 1,                        /* Type 1 Format */
		.bm_formats                 = 0x00000001,               /* Type 1 - PCM format */
		.b_nr_channels              = USB_NUM_AUDIO_CHANNELS,/* Number of Channels */
		.bm_channel_config          = ((1 << USB_NUM_AUDIO_CHANNELS)-1), /* Channel Config */
		.p_encoder_descriptor       = CLD_NULL,
		.p_decoder_descriptor       = CLD_NULL,
		.p_format_descriptor        = (unsigned char*)USB_in_stream_format_descriptor,
		.p_audio_stream_endpoint_data_descriptor = (CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor*)&USB_in_stream_endpoint_desc,
};
/*
 * ----------------------------------------------------------------
 * 		Audio Stream OUT Interface parameters
 * ---------------------------------------------------------------
 */
static CLD_SC58x_Audio_2_0_Stream_Interface_Params USB_out_endpoint_params =
{
		.endpoint_number            = 3,                        			/* Isochronous endpoint number */
		.max_packet_size_full_speed = USER_AUDIO_MAX_PACKET_SIZE,			/* Isochronous endpoint full-speed max packet size */
		.max_packet_size_high_speed = USER_AUDIO_MAX_PACKET_SIZE,			/* Isochronous endpoint high-speed max packet size */
		.b_interval_full_speed      = 1,                        			/* Isochronous endpoint full-speed bInterval */
		.b_interval_high_speed      = 2,                        			/* Isochronous endpoint high-speed bInterval */
		.b_terminal_link            = 1,                        			/* Terminal ID of the associated Output Terminal */
		.b_format_type              = 1,                        			/* Type 1 Format */
		.bm_formats                 = 0x00000001,               			/* Type 1 - PCM format */
		.b_nr_channels              = USB_NUM_AUDIO_CHANNELS, 				/* Number of Channels */
		.bm_channel_config          = ((1 << USB_NUM_AUDIO_CHANNELS)-1), 	/* Channel Config */
		.p_encoder_descriptor       = CLD_NULL,
		.p_decoder_descriptor       = CLD_NULL,
		.p_format_descriptor        = (unsigned char*)USB_out_stream_format_descriptor,
		.p_audio_stream_endpoint_data_descriptor = (CLD_SC58x_Audio_2_0_Audio_Stream_Data_Endpoint_Descriptor*)&USB_out_stream_endpoint_desc,
};

/*
 * ----------------------------------------------------------------
 * 		Audio Control Interrupt IN endpoint parameters
 * ---------------------------------------------------------------
 */
static CLD_SC58x_Audio_2_0_Control_Interrupt_Params user_audio_interrupt_in_params =
{
		.endpoint_number            = 1,                        /* Endpoint number */
		.b_interval_full_speed      = 1,                        /* Interrupt IN endpoint full-speed bInterval */
		.b_interval_high_speed      = 4,                        /* Interrupt IN endpoint high-speed bInterval - 1 millisecond */
};

/*
 * ----------------------------------------------------------------
 * 		Rate feedback parameters
 * ---------------------------------------------------------------
 */
static CLD_SC58x_Audio_2_0_Rate_Feedback_Params USB_rate_feedback_params =
{
		/* Parameters used for the Isochronous Rate Feedback Endpoint Descriptor */
		.max_packet_size_full_speed = 32,
		.max_packet_size_high_speed = 32,
		.b_interval_full_speed      = 1,                        /* Isochronous endpoint full-speed bInterval */
		.b_interval_high_speed      = 4,                        /* Isochronous endpoint high-speed bInterval - 1 millisecond */
};

/*
 * ----------------------------------------------------------------
 * 		CLD SC58x Audio 2.0 library initialization data.
 * ---------------------------------------------------------------
 */
static CLD_SC58x_Audio_2_0_Lib_Init_Params USB_init_params =
{
		.usb_config                                         = CLD_USB0_AUDIO,
		.enable_dma                                         = CLD_TRUE,

		.vendor_id                                          = USB_VENDOR_ID,
		.product_id                                         = USB_PRODUCT_ID,
		.usb_bus_max_power                                  = 0,
		.device_descriptor_bcdDevice                        = 0x0100,

		.audio_control_category_code                        = 0x0a,                /* Pro Audio */
		.p_audio_control_interrupt_params                   = CLD_NULL,//&user_audio_interrupt_in_params,

		.p_unit_and_terminal_descriptors                    = (unsigned char*)USB_unit_and_terminal_descriptor,
		.unit_and_terminal_descriptors_length               = sizeof(USB_unit_and_terminal_descriptor),

		.p_audio_streaming_rx_interface_params              = &USB_out_endpoint_params,
		.p_audio_rate_feedback_rx_params                    = &USB_rate_feedback_params,

		.p_audio_streaming_tx_interface_params              = &USB_in_endpoint_params,

		.fp_audio_stream_data_received                      = USB_data_received,

		.fp_audio_set_req_cmd                               = user_audio_set_req_cmd,
		.fp_audio_get_req_cmd                               = user_audio_get_req_cmd,

		.fp_audio_streaming_rx_endpoint_enabled             = user_audio_streaming_rx_endpoint_enabled,
		.fp_audio_streaming_tx_endpoint_enabled             = user_audio_streaming_tx_endpoint_enabled,

		.p_usb_string_manufacturer                          = "Kardome Technologies",
		.p_usb_string_product                               = "GAVEL Rev 1.0",
		.p_usb_string_serial_number                         = CLD_NULL,
		.p_usb_string_configuration                         = CLD_NULL,

		.p_usb_string_audio_control_interface               = CLD_NULL,
		.p_usb_string_audio_streaming_out_interface         = "Audio 2.0 Output",
		.p_usb_string_audio_streaming_in_interface          = "Audio 2.0 Input",

		.user_string_descriptor_table_num_entries           = 0,
		.p_user_string_descriptor_table                     = NULL,

		.usb_string_language_id                             = 0x0409,       /* English (US) language ID */

		.fp_cld_usb_event_callback                          = USB_event,

		.fp_cld_lib_status                                  = user_cld_lib_status,
};

static CLD_USB_Audio_Feedback_Params feedback_transfer_data =
{
		.fp_transfer_aborted_callback = user_audio_feedback_xfr_done,
		.transfer_timeout_ms = 1100,
		.fp_usb_in_transfer_complete = user_audio_feedback_xfr_done,
};

#pragma pack(1)
#define USB_AUDIO_MAX_NUM_SUB_RANGES     1  /*!< Maximum number of sub-range values the device supports */

/*
 * ----------------------------------------------------------------
 * 		Structure used to report 32-bit USB Audio 2.0 range values.
 * ----------------------------------------------------------------
 */
typedef struct
{
	unsigned short  w_num_sub_ranges;   /*!< The number of sub ranges being reported */
	struct
	{
		unsigned long   d_min;          /*!< Range Minimum */
		unsigned long   d_max;          /*!< Range Minimum */
		unsigned long   d_res;          /*!< Range Minimum */
	} sub_ranges[USB_AUDIO_MAX_NUM_SUB_RANGES]; /*!< Array of supported sub-ranges */
} USB_Audio_4_byte_Range_Response;

/**
 * ----------------------------------------------------------------
 * 		Structure used to report 16-bit USB Audio 2.0 range values.
 * ----------------------------------------------------------------
 */
typedef struct
{
	unsigned short  w_num_sub_ranges;   /*!< The number of sub-ranges being reported */
	struct
	{
		unsigned short  d_min;          /*!< Range Minimum */
		unsigned short  d_max;          /*!< Range Minimum */
		unsigned short  d_res;          /*!< Range Minimum */
	} sub_ranges[USB_AUDIO_MAX_NUM_SUB_RANGES]; /*!< Array of supported sub-ranges */
} USB_Audio_2_byte_Range_Response;

static USB_Audio_2_byte_Range_Response user_audio_2_byte_range_resp;

#pragma pack()

/*
 * ----------------------------------------------------------------
 * 		Structure used to store the current USB volume settings.
 * ----------------------------------------------------------------
 */
typedef struct
{
	unsigned short master;              /*!< Master Volume setting */
	unsigned short vol[USB_NUM_AUDIO_CHANNELS];
	unsigned short min;                 /*!< Volume range minimum setting */
	unsigned short max;                 /*!< Volume range maximum setting */
	unsigned short resolution;          /*!< Volume range resolution */
	unsigned char mute;                 /*!< Mute setting */
} User_Audio_Data_Volume;

/*
 * ----------------------------------------------------------------
 * 		Structure used to store the requested USB volume settings.
 * ----------------------------------------------------------------
 */
typedef struct
{
	unsigned short vol[USB_NUM_AUDIO_CHANNELS];
	unsigned char mute;                 /*!< Mute setting */
} User_Audio_Req_Volume;

/*
 * ----------------------------------------------------------------
 * 		User Audio data
 * ----------------------------------------------------------------
 */
typedef struct
{
	CLD_Boolean             volume_changed;                 /*!< CLD_TRUE when a Set Volume or Mute command is received */
	User_Audio_Data_Volume  headphone_output_volume;        /*!< Current Volume settings */
	User_Audio_Req_Volume   headphone_req_output_volume;    /*!< Requested Volume settings */
	User_Audio_Data_Volume  mic_input_volume;               /*!< Current Microphone Volume Settings */
	User_Audio_Req_Volume   microphone_req_input_volume;    /*!< Requested Microphone Volume */
	CLD_Boolean 			isochronous_in_enabled;         /*!< CLD_TRUE - Isochronous IN endpoint enabled */
	CLD_Boolean 			isochronous_in_idle;            /*!< CLD_TRUE - Endpoint is idle so data can be transmitted. */
	unsigned long 			clock_sample_rate;              /*!< Active audio sample rate */
	CLD_Boolean 			isochronous_out_enabled;        /*!< CLD_TRUE - Isochronous OUT endpoint enabled */
	CLD_Boolean 			rate_feedback_idle;
} User_Audio_Data;

/*
 * ----------------------------------------------------------------
 *  		Locate the User_Audio_Data in un-cached memory since USB DMA will be used to access the
 *  		audio data buffers.
 * ----------------------------------------------------------------
 */
static User_Audio_Data user_audio_data =
{
		.volume_changed = CLD_FALSE,
		.headphone_output_volume.min = 0xa060,                  /* ADAU1962A USB Minimum Volume Setting = -95.6250dB */
		.headphone_output_volume.max = 0x0000,                  /* ADAU1962A USB Maximum Volume Setting = 0dB */
		.headphone_output_volume.resolution = 0x0060,           /* ADAU1962A USB Volume Resolution Setting = 0.3750dB */

		.mic_input_volume.min = 0xDC60,                         /* ADAU1979 USB Minimum Volume Setting = -35.625dB */
		.mic_input_volume.max = 0x3C00,                         /* ADAU1979 USB Maximum Volume Setting = 60dB */
		.mic_input_volume.resolution = 0x0060,                  /* ADAU1979 USB Volume Resolution Setting = 0.3750dB */

		.isochronous_in_enabled = CLD_FALSE,
		.isochronous_in_idle = CLD_TRUE,
		.clock_sample_rate = 48000,

		.isochronous_out_enabled = CLD_FALSE,
		.rate_feedback_idle = CLD_TRUE,
};

/*
 * ----------------------------------------------------------------
 *  		Clock Source sample rate range.
 * ----------------------------------------------------------------
 */
static USB_Audio_4_byte_Range_Response user_audio_get_clock_sample_rate_range_resp =
{
		.w_num_sub_ranges 		= 1,
		.sub_ranges[0].d_min 	= 48000,
		.sub_ranges[0].d_max 	= 48000,
		.sub_ranges[0].d_res 	= 1,
};
//=================================================================
//=================================================================
//=================================================================
//
//			The following section should be HERE!!! -
// 			Otherwise memory allocation problem is expected
// 			AS 11.2.2019
//
//=================================================================
//=================================================================
//=================================================================
static CLD_Boolean 				first_feedback 	= CLD_TRUE;
T_USB_configuration 			USB_configuration;
int* 							p_USB_pc_data_buffer;

//-------------------------------------------
// 		USB Tx queue
//-------------------------------------------
static unsigned char* 			USB_TX_QUEUE_pointers[USB_TX_QUEUE_SIZE];
static unsigned int 			USB_TX_QUEUE_sizes_bytes[USB_TX_QUEUE_SIZE];
static int 						USB_TX_QUEUE_in_index;
static int 						USB_TX_QUEUE_out_index;
//-------------------------------------------
// 		USB Rx Ping/Pong buffers
//-------------------------------------------
int* 							p_USB_TX_to_USB_PING 		= (int*)ADDRESS_TX_to_USB_PING;
int* 							p_USB_TX_to_USB_PONG 		= (int*)ADDRESS_TX_to_USB_PONG;
unsigned char* 					p_USB_RX_from_USB_PING 		= (unsigned char*)ADDRESS_RX_from_USB_PING;
unsigned char* 					p_USB_RX_from_USB_PONG 		= (unsigned char*)ADDRESS_RX_from_USB_PONG;

unsigned int 					USB_RX_from_USB_number_of_bytes_received;
unsigned int 					USB_RX_from_USB_pingpong_index;
//-------------------------------------------
// 		PC data
//-------------------------------------------
float* 							p_USB_PC_data_buffer 		= (float*)ADDRESS_PC_data_buffer;
unsigned int					USB_PC_number_of_bytes_received;

int 							ALON[1024];
float 							AMIR[1024];


/*--------------------------------------------
 *  USB_init
 * 	Alon Slapak 22.1.2019
 * 	Initializes the CLD SC58x USB Audio 2.0 library.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_init
 *	Input:
 *		void
 * 	Output:
 *  	USB_RESULT:
 *  		USB_INIT_SUCCESS
 *  		USB_INIT_ONGOING
 *  		USB_INIT_FAILED
 * --------------------------------------------
 */
USB_RESULT USB_init(T_USB_configuration* p_USB_configuration)
{
	CLD_RV CLD_rv 									= CLD_ONGOING;
	USB_configuration.FLAG_loopback 				= p_USB_configuration->FLAG_loopback;
	USB_configuration.SPORT_buffer_length_samples 	= p_USB_configuration->SPORT_buffer_length_samples;



	int k;
	for (k = 0 ; k<1024 ; k++)
	{
		ALON[k] = k << 16;
		AMIR[k] = -(float)1.0/(float)(k+1);
	}
	/*---------------------------------------------------------
	 *  		 Configure sampling rate
	 *---------------------------------------------------------*/
	user_audio_data.clock_sample_rate 		= p_USB_configuration->Sampling_frequency_Hz;
	user_audio_get_clock_sample_rate_range_resp.sub_ranges[0].d_min = p_USB_configuration->Sampling_frequency_Hz;
	user_audio_get_clock_sample_rate_range_resp.sub_ranges[0].d_max = p_USB_configuration->Sampling_frequency_Hz;
	feedback_transfer_data.desired_data_rate = p_USB_configuration->Sampling_frequency_Hz / 1000;
	/*---------------------------------------------------------
	 *  		 Initialize the CLD Audio v2.0 with CDC Library
	 *---------------------------------------------------------*/
	CLD_rv = cld_sc58x_audio_2_0_lib_init(&USB_init_params);

	if (CLD_rv == CLD_SUCCESS)
	{
		/*---------------------------------------------------------
		 *  		Register and enable the USB interrupt ISR functions
		 *---------------------------------------------------------*/
		adi_int_InstallHandler(USB0_STAT_IRQn, user_audio_usb0_isr, NULL, 1);
		adi_int_InstallHandler(USB0_DATA_IRQn, user_audio_usb0_isr, NULL, 1);
		/*---------------------------------------------------------
		 *  		Initialize the RX_data_from_USB PING/PONG buffers
		 *---------------------------------------------------------*/
		USB_RX_from_USB_pingpong_index 			= 0;
		USB_RX_from_USB_number_of_bytes_received 		= 0;
		/*---------------------------------------------------------
		 *  		Initialize the Tx queue
		 *---------------------------------------------------------*/
		USB_TX_QUEUE_in_index 	= 0;
		USB_TX_QUEUE_out_index 	= -1;
		//    	/*---------------------------------------------------------
		//    	 *  		TODO 31.1.2019: understand the volume and clean up
		//    	 *---------------------------------------------------------*/
		//    	for (i = 4; i < USB_NUM_AUDIO_CHANNELS; i++)
		//    	{
		//    		user_audio_data.microphone_req_input_volume.vol[i] = user_audio_data.mic_input_volume.vol[0];
		//    	}
		/*---------------------------------------------------------
		 *  		 Connect to the USB Host
		 *---------------------------------------------------------*/
		cld_lib_usb_connect();
		return USB_INIT_SUCCESS;
	}
	else if (CLD_rv == CLD_FAIL)
	{
		return USB_INIT_FAILED;
	}
	else
	{
		return USB_INIT_ONGOING;
	}
}


/*--------------------------------------------
 *  USB_main
 * 	Alon Slapak 22.1.2019
 * 	Invokes the CLD library mainline function, which is
 * 	required and should be called in each iteration of the main program loop.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_main
 *	Input:
 *		void
 * 	Output:
 *  	void
 *
 * --------------------------------------------
 */
void USB_main (void)
{
	cld_sc58x_audio_2_0_lib_main();
}


/**
 * Transmits the current feedback value to the USB Host.
 *
 */
static void user_audio_tx_feedback_data (void)
{
	//    static CLD_USB_Audio_Feedback_Params feedback_transfer_data =
	//    {
	//        .fp_transfer_aborted_callback = user_audio_feedback_xfr_done,
	//        .transfer_timeout_ms = 1100,
	//        .fp_usb_in_transfer_complete = user_audio_feedback_xfr_done,
	//    };

	adi_rtl_disable_interrupts();
	if ((user_audio_data.isochronous_out_enabled == CLD_TRUE) && (user_audio_data.rate_feedback_idle == CLD_TRUE))
	{
		feedback_transfer_data.desired_data_rate = 48;
		if (cld_sc58x_audio_2_0_lib_transmit_audio_rate_feedback_data(&feedback_transfer_data)== CLD_USB_TRANSMIT_SUCCESSFUL)
		{
			user_audio_data.rate_feedback_idle = CLD_FALSE;
		}
	}
	adi_rtl_reenable_interrupts();
}



/*--------------------------------------------
 *  USB_data_received
 * 	Alon Slapak 27.1.2019
 * This function is called by the cld_sc58x_audio_2_0_w_cdc_lib library when data is
 * received on the Isochronous OUT endpoint. This function sets the
 * p_transfer_data parameters to select where the received data
 * should be stored, and what function should be called when the
 * transfer is complete.
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_stream_data_received
 *	Input:
 *		p_transfer_data                - Pointer to the Isochronous OUT transfer data.
 * 		p_transfer_data->num_bytes     - Number of received OUT bytes.
 *                                         This value can be set to the total
 *                                         transfer size if more then one
 *                                         packet is expected.
 *		p_transfer_data->p_data_buffer - Set to the address where the
 *                                         data should be written.
 *		p_transfer_data->callback.fp_usb_out_transfer_complete -
 *                                         Function called when the
 *                                         requested received bytes have
 *                                         been written to p_data_buffer.
 *		p_transfer_data->fp_transfer_aborted_callback -
 *                                         Optional function that is
 *                                         called if the transfer is
 *                                         aborted.
 * 		p_transfer_data->transfer_timeout_ms - Transfer timeout in milliseconds.
 *                                         Set to 0 to disable timeout.
 * 	Output:
 *  	CLD_USB_TRANSFER_ACCEPT 	- Store the data using the p_transfer_data
 *                                  	parameters..
 * 		CLD_USB_TRANSFER_PAUSE 		- The device isn't ready to process this
 *                                  	out packet so pause the transfer
 *                                  	until the cld_sc58x_audio_2_0_w_cdc_lib_resume_paused_audio_data_transfer
 *                                  	function is called.
 * 		CLD_USB_TRANSFER_DISCARD 	- Discard this packet.
 * 		CLD_USB_TRANSFER_STALL 		- Stall the OUT endpoint.
 * --------------------------------------------
 */
static CLD_USB_Transfer_Request_Return_Type USB_data_received (CLD_USB_Transfer_Params* p_transfer_data)
{
	//----------------------------------------------
	// 			Loop back mode
	// 	Working with a single circular buffer
	//----------------------------------------------
	if (USB_configuration.FLAG_loopback)
	{
//		//----------------------------------------------
//		// 		Ping/Pong
//		//----------------------------------------------
//		if (USB_RX_from_USB_pingpong_index == 0)
//		{
//			p_transfer_data->p_data_buffer 						= (unsigned char*)&p_USB_RX_from_USB_PING[USB_RX_from_USB_number_of_bytes_received];
//		}
//		else
//		{
//			p_transfer_data->p_data_buffer 						= (unsigned char*)&p_USB_RX_from_USB_PONG[USB_RX_from_USB_number_of_bytes_received];
//		}
//		//----------------------------------------------
//		//
//		//----------------------------------------------
//		USB_PC_number_of_bytes_received 						= p_transfer_data->num_bytes;
//		p_transfer_data->transfer_timeout_ms 					= 0;
//		p_transfer_data->fp_transfer_aborted_callback 			= CLD_NULL;
//		p_transfer_data->callback.fp_usb_out_transfer_complete 	= USB_data_receive_completed;

		//----------------------------------------------
		// 		Ping/Pong
		//----------------------------------------------
		if (USB_RX_from_USB_pingpong_index == 0)
		{
			p_transfer_data->p_data_buffer 						= p_USB_RX_from_USB_PING;
			USB_RX_from_USB_pingpong_index 						= 1;
		}
		else
		{
			p_transfer_data->p_data_buffer 						= p_USB_RX_from_USB_PONG;
			USB_RX_from_USB_pingpong_index 						= 0;
		}
		//----------------------------------------------
		//
		//----------------------------------------------
		USB_PC_number_of_bytes_received 						= p_transfer_data->num_bytes;
		p_transfer_data->transfer_timeout_ms 					= 0;
		p_transfer_data->fp_transfer_aborted_callback 			= CLD_NULL;
		p_transfer_data->callback.fp_usb_out_transfer_complete 	= USB_data_receive_completed;

		return CLD_USB_TRANSFER_ACCEPT;
	}
	//----------------------------------------------
	// 			Receive data from PC
	// 	Working with a PING/PONG buffers
	//----------------------------------------------
	else
	{
		//----------------------------------------------
		// 		Ping/Pong
		//----------------------------------------------
		if (USB_RX_from_USB_pingpong_index == 0)
		{
			p_transfer_data->p_data_buffer 						= p_USB_RX_from_USB_PING;
			USB_RX_from_USB_pingpong_index 						= 1;
		}
		else
		{
			p_transfer_data->p_data_buffer 						= p_USB_RX_from_USB_PONG;
			USB_RX_from_USB_pingpong_index 						= 0;
		}
		//----------------------------------------------
		//
		//----------------------------------------------
		USB_PC_number_of_bytes_received 						= p_transfer_data->num_bytes;
		p_transfer_data->transfer_timeout_ms 					= 0;
		p_transfer_data->fp_transfer_aborted_callback 			= CLD_NULL;
		p_transfer_data->callback.fp_usb_out_transfer_complete 	= USB_data_receive_completed;

		return CLD_USB_TRANSFER_ACCEPT;
	}
}

/*--------------------------------------------
 *  USB_data_receive_completed
 * 	Alon Slapak 27.1.2019
 * Function called when headphone data has been received.
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_stream_data_receive_complete
 *	Input:
 *		void
 * 	Output:
 *  	CLD_USB_Data_Received_Return_Type
 *  		CLD_USB_DATA_GOOD
 *  		CLD_USB_DATA_BAD_STALL
 *
 *  Packet/message structure:
 *  	A USB message is 24 X 32-bit word (=96 bytes) with the following fields:
 *  		0 		Signature 			USB_PC_DATA_BUFFER_SIGNATURE
 *  		1 		Serial number
 *  		2 		Command
 *  		3 		Address
 *  		4-24	Data
 *
 *  	A packet is usually 4 messages (=384 bytes), but may be 5 messages (=480 bytes)
 *
 *  When a packet is received, the following actions are taken
 *  	1. Finding the beginning of a message in the packet
 *  	2. Execute the command
 *
 *  Commands:
 *  	100 	receive data from PC
 *  	200		send data to PC
 *  	1100	LED2 ON
 *  	1200	LED2 OFF
 *  	1300	LED3 ON
 *  	1400	LED3 OFF
 *  	1500	LED4 ON
 *  	1600	LED4 OFF
 * --------------------------------------------
 */
static CLD_USB_Data_Received_Return_Type USB_data_receive_completed(void)
{
	//----------------------------------------------------
	// 		Loopback mode
	//----------------------------------------------------
	if (USB_configuration.FLAG_loopback)
	{
		unsigned int   buffer_length_bytes 					= USB_configuration.SPORT_buffer_length_samples *  GVL_NUM_AUDIO_CHANNELS * GVL_SAMPLE_SIZE_BYTES;
		//----------------------------------------
		// 			Index
		//----------------------------------------
		USB_RX_from_USB_number_of_bytes_received 			+= USB_PC_number_of_bytes_received;
		//----------------------------------------
		// 			Buffer is full
		//----------------------------------------
		if (USB_RX_from_USB_number_of_bytes_received > buffer_length_bytes)
		{
			//----------------------------------------------
			// 		Ping
			//----------------------------------------------
			if (USB_RX_from_USB_pingpong_index == 0)
			{
				//--------------------------------------
				// 	 Cyclic index
				//--------------------------------------
				memcpy((char*)p_USB_RX_from_USB_PONG, (char*)&p_USB_RX_from_USB_PING[buffer_length_bytes], USB_RX_from_USB_number_of_bytes_received - buffer_length_bytes);
				USB_RX_from_USB_number_of_bytes_received 	-= buffer_length_bytes;
				USB_RX_from_USB_pingpong_index 				= 1;
				//--------------------------------------
				// 	 Process the audio samples
				//--------------------------------------
				*pREG_SEC0_RAISE = (INTR_SOFT2 - ADI_INTR_BASE);
				//--------------------------------------
				// 	 Send processed buffer to USB
				//--------------------------------------
				USB_transmit_data(p_USB_TX_to_USB_PING,  USB_configuration.SPORT_buffer_length_samples *  GVL_NUM_AUDIO_CHANNELS);
			}
			//----------------------------------------------
			// 		Pong
			//----------------------------------------------
			else
			{
				//--------------------------------------
				// 	 Cyclic index
				//--------------------------------------
				memcpy((char*)p_USB_RX_from_USB_PING, (char*)&p_USB_RX_from_USB_PONG[buffer_length_bytes], USB_RX_from_USB_number_of_bytes_received - buffer_length_bytes);
				USB_RX_from_USB_number_of_bytes_received 	-= buffer_length_bytes;
				USB_RX_from_USB_pingpong_index 				= 0;
				//--------------------------------------
				// 	 Process the audio samples
				//--------------------------------------
				*pREG_SEC0_RAISE = (INTR_SOFT3 - ADI_INTR_BASE);
				//--------------------------------------
				// 	 Send processed buffer to USB
				//--------------------------------------
				USB_transmit_data(p_USB_TX_to_USB_PONG,  USB_configuration.SPORT_buffer_length_samples *  GVL_NUM_AUDIO_CHANNELS);
			}
		}
	}
	//----------------------------------------------------
	// 		Receive data from PC
	//----------------------------------------------------
	else
	{
		int* 	p_PC_packet;
		int		SHARC_PC_data_packet_address;
		//----------------------------------------------------------------
		// 		Ping-Pong buffer
		//----------------------------------------------------------------
//		if (USB_RX_from_USB_pingpong_index == 0)
//		{
//			p_PC_packet = (int*)&p_USB_RX_from_USB_PONG;
//		}
//		else
//		{
//			p_PC_packet = (int*)&p_USB_RX_from_USB_PING;
//		}
//		//----------------------------------------------------------------
//		// 		Parsing
//		//----------------------------------------------------------------
//		unsigned int 	counter = 0;
//		while (counter < USB_PC_number_of_bytes_received)
//		{
//			//----------------------------------------------------------------
//			// 		Check signature message. Remove last 4 bits because of noise
//			//----------------------------------------------------------------
//			if ((p_PC_packet[counter] & USB_INTEGER_ACCURACY_MASK) >> 8 ==  USB_PC_DATA_BUFFER_SIGNATURE)
//			{
//				//----------------------------------------------------------------
//				// 		Operation (remove noise in last 8 bits)
//				//----------------------------------------------------------------
//				switch ((p_PC_packet[counter + 2] & USB_INTEGER_ACCURACY_MASK) >> 8)
//				{
//				//----------------------------------------------------------------
//				//
//				// 		Receive data: 		Copy data from message to the array
//				//
//				//----------------------------------------------------------------
//				case 100:
//					//------------------------------------------------------------
//					// 		Index in array (remove noise in last 8 bits)
//					//------------------------------------------------------------
//					SHARC_PC_data_packet_address 		= (p_PC_packet[counter + 3]  & USB_INTEGER_ACCURACY_MASK) >> 8;
//					//------------------------------------------------------------
//					// 		Data: message --> Array
//					//------------------------------------------------------------
//					memcpy((char*)(&p_USB_PC_data_buffer[SHARC_PC_data_packet_address]), (char*)(&p_PC_packet[counter + 4]) ,20 * 4);
//				break;
//				//----------------------------------------------------------------
//				//
//				// 		Transmit data: 		Copy data from the array to the message
//				//
//				//----------------------------------------------------------------
//				case 200:
//					//------------------------------------------------------------
//					// 		Index in array (remove noise in last 8 bits)
//					//------------------------------------------------------------
//					SHARC_PC_data_packet_address 		= (p_PC_packet[counter + 3]  & USB_INTEGER_ACCURACY_MASK) >> 8;
//					//------------------------------------------------------------
//					// 		Data: Array --> message
//					//------------------------------------------------------------
//					memcpy((char*)(&p_PC_packet[counter + 4]) ,(char*)(&p_USB_PC_data_buffer[SHARC_PC_data_packet_address]), 20 * 4);
//				break;
//				//----------------------------------------------------------------
//				// 		LEDs
//				//----------------------------------------------------------------
//				case 1100:
//						LED_2_on();
//				break;
//				case 1200:
//						LED_2_off();
//				break;
//				case 1300:
//				 		LED_3_on();
//				break;
//				case 1400:
//						LED_3_off();
//				break;
//				case 1500:
//						LED_4_on();
//				break;
//				case 1600:
//						LED_4_off();
//				break;
//				}
//				counter += 24;
//			}
//			//----------------------------------------------------------------
//			// 		No packet-start found
//			//----------------------------------------------------------------
//			else
//			{
//				counter++;
//			}
//		}
//		//----------------------------------------------------------------
//		// 		Send the packet back
//		//----------------------------------------------------------------
//		if (USB_RX_from_USB_pingpong_index == 0)
//		{
//			USB_transmit_data(&p_USB_RX_from_USB_PONG, USB_PC_number_of_bytes_received);
//		}
//		else
//		{
//			USB_transmit_data(&p_USB_RX_from_USB_PING, USB_PC_number_of_bytes_received);
//		}
	}
	return CLD_USB_DATA_GOOD;
}

/**
 * This function is called by the CLD Audio v2.0 library when a Set Command
 * request is received. This function sets the p_transfer_data parameters to
 * select where the received data should be stored, and what function should
 * be called when the transfer is complete.
 *
 * @param p_transfer_data                - Pointer to the Control OUT transfer data.
 * @param p_transfer_data->num_bytes     - Number of Control OUT bytes.
 * @param p_transfer_data->p_data_buffer - Set to the address where to
 *                                         store the Control OUT data.
 * @param p_transfer_data->callback.usb_in_transfer_complete -
 *                                         Function called when the
 *                                         Control OUT bytes have been
 *                                         received.
 * @param p_transfer_data->transfer_aborted_callback -
 *                                         Optional function that is
 *                                         called if the transfer is
 *                                         aborted.
 * @param p_req_params->req              - Setup Packet bRequest value.
 * @param p_req_params->entity_id        - Requested entity ID (Unit ID,
 *                                         Terminal ID, etc)
 * @param p_req_params->interface_or_endpoint_num -
 *                                         Requested interface or endpoint
 *                                         number depending on the entity.
 * @param p_req_params->setup_packet_wValue
 *
 * @retval CLD_USB_TRANSFER_ACCEPT - Store the data using the p_transfer_data
 *                                   parameters.
 * @retval CLD_USB_TRANSFER_PAUSE - The device isn't ready to process this
 *                                  out packet so pause the transfer
 *                                  until the cld_sc58x_audio_2_0_w_cdc_lib_resume_paused_control_transfer
 *                                  function is called.
 * @retval CLD_USB_TRANSFER_DISCARD - Discard this packet.
 * @retval CLD_USB_TRANSFER_STALL - Stall the OUT endpoint.
 *
 */
static CLD_USB_Transfer_Request_Return_Type user_audio_set_req_cmd (CLD_SC58x_Audio_2_0_Cmd_Req_Parameters * p_req_params, CLD_USB_Transfer_Params * p_transfer_data)
{
	CLD_USB_Transfer_Request_Return_Type rv = CLD_USB_TRANSFER_DISCARD;

	/* If the USB Host is changing the Headphone Volume */
	if (p_req_params->entity_id == USER_AUDIO_HEADPHONE_FEATURE_UNIT_ID)
	{
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			if (p_req_params->setup_packet_wValue == USER_AUDIO_HEADPHONE_CONTROL_AND_CHANNEL_NUM_MASTER) /* Master Volume */
			{
				/* Store the volume setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_req_output_volume.vol[0];
				p_transfer_data->num_bytes = 2;
			}
			else if (((p_req_params->setup_packet_wValue & 0xff00) == 0x0200) &&
					((p_req_params->setup_packet_wValue & 0x00ff) <= USB_NUM_AUDIO_CHANNELS))
			{
				/* Store the volume setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_req_output_volume.vol[(p_req_params->setup_packet_wValue & 0x00ff)-1];
				p_transfer_data->num_bytes = 2;
			}
			else /* Mute */
			{
				/* Store the mute setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_req_output_volume.mute;
				p_transfer_data->num_bytes = 1;
			}
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_out_transfer_complete = user_audio_set_volume_req;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	/* If the USB Host is changing the Microphone Volume */
	else if (p_req_params->entity_id == USER_AUDIO_MICROPHONE_FEATURE_UNIT_ID)
	{
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			if (p_req_params->setup_packet_wValue == USER_AUDIO_HEADPHONE_CONTROL_AND_CHANNEL_NUM_MASTER) /* Master Volume */
			{
				/* Store the volume setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.microphone_req_input_volume.vol[0];
				p_transfer_data->num_bytes = 2;
			}
			else if (((p_req_params->setup_packet_wValue & 0xff00) == 0x0200) &&
					((p_req_params->setup_packet_wValue & 0x00ff) <= USB_NUM_AUDIO_CHANNELS))
			{
				/* Store the volume setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.microphone_req_input_volume.vol[(p_req_params->setup_packet_wValue & 0x00ff)-1];
				p_transfer_data->num_bytes = 2;
			}
			else /* Mute */
			{
				/* Store the mute setting in the headphone_req_output_volume structure */
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.microphone_req_input_volume.mute;
				p_transfer_data->num_bytes = 1;
			}
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_out_transfer_complete = user_audio_set_volume_req;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	return rv;
}

/**
 * This function is called by the CLD Audio library when a Set Volume request data
 * has been received.
 *
 * @retval CLD_USB_DATA_GOOD - Received data is valid.
 * @retval CLD_USB_DATA_BAD_STALL - Received data is invalid.
 */
static CLD_USB_Data_Received_Return_Type user_audio_set_volume_req (void)
{
	/* Flag that new volume settings have been received. */
	user_audio_data.volume_changed = CLD_TRUE;
	return CLD_USB_DATA_GOOD;
}

/**
 * This function is called by the CLD Audio v2.0 library when a Get Command
 * request is received. This function sets the p_transfer_data parameters to
 * select where the transmit data should be sourced, and what function should
 * be called when the transfer is complete.
 *
 * @param p_transfer_data                - Pointer to the Control IN transfer data.
 * @param p_transfer_data->num_bytes     - Number of Control IN bytes.
 * @param p_transfer_data->p_data_buffer - Set to the address where to
 *                                         source the Control IN data.
 * @param p_transfer_data->callback.usb_in_transfer_complete -
 *                                         Function called when the
 *                                         Control IN bytes have been
 *                                         sent.
 * @param p_transfer_data->transfer_aborted_callback -
 *                                         Optional function that is
 *                                         called if the transfer is
 *                                         aborted.
 * @param p_req_params->req              - Setup Packet bRequest value.
 * @param p_req_params->entity_id        - Requested entity ID (Unit ID,
 *                                         Terminal ID, etc)
 * @param p_req_params->interface_or_endpoint_num -
 *                                         Requested interface or endpoint
 *                                         number depending on the entity.
 * @param p_req_params->setup_packet_wValue
 *
 * @retval LD_USB_TRANSFER_ACCEPT - Store the data using the p_transfer_data
 *                                  parameters.
 * @retval CLD_USB_TRANSFER_PAUSE - The device isn't ready to process this
 *                                  out packet so pause the transfer
 *                                  until the cld_sc58x_audio_2_0_w_cdc_lib_resume_paused_control_transfer
 *                                  function is called.
 * @retval CLD_USB_TRANSFER_DISCARD - Discard this packet.
 * @retval CLD_USB_TRANSFER_STALL - Stall the OUT endpoint.
 *
 */
static CLD_USB_Transfer_Request_Return_Type user_audio_get_req_cmd (CLD_SC58x_Audio_2_0_Cmd_Req_Parameters * p_req_params, CLD_USB_Transfer_Params * p_transfer_data)
{
	CLD_USB_Transfer_Request_Return_Type rv = CLD_USB_TRANSFER_DISCARD;

	/* If the USB Host is Requesting the Headphone Volume */
	if (p_req_params->entity_id == USER_AUDIO_HEADPHONE_FEATURE_UNIT_ID)
	{
		/* Current Setting */
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			if (p_req_params->setup_packet_wValue == USER_AUDIO_HEADPHONE_CONTROL_AND_CHANNEL_NUM_MASTER) /* Master Volume */
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_output_volume.vol[0];
				p_transfer_data->num_bytes = 2;
			}
			else if (((p_req_params->setup_packet_wValue & 0xff00) == 0x0200) &&
					((p_req_params->setup_packet_wValue & 0x00ff) <= USB_NUM_AUDIO_CHANNELS))
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_output_volume.vol[(p_req_params->setup_packet_wValue & 0x00ff)-1];
				p_transfer_data->num_bytes = 2;
			}
			else /* Mute */
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.headphone_output_volume.mute;
				p_transfer_data->num_bytes = 1;
			}

			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
		/* Headphone Range Settings */
		else if (p_req_params->req == CLD_REQ_RANGE)
		{
			user_audio_2_byte_range_resp.w_num_sub_ranges = 1;
			user_audio_2_byte_range_resp.sub_ranges[0].d_max = user_audio_data.headphone_output_volume.max;
			user_audio_2_byte_range_resp.sub_ranges[0].d_min = user_audio_data.headphone_output_volume.min;
			user_audio_2_byte_range_resp.sub_ranges[0].d_res = user_audio_data.headphone_output_volume.resolution;

			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_2_byte_range_resp;
			p_transfer_data->num_bytes = sizeof(user_audio_2_byte_range_resp);
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	/* If the USB Host is Requesting the Microphone Volume */
	else if (p_req_params->entity_id == USER_AUDIO_MICROPHONE_FEATURE_UNIT_ID)
	{
		/* Current microphone Volume */
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			if (p_req_params->setup_packet_wValue == USER_AUDIO_MICROPHONE_CONTROL_AND_CHANNEL_NUM_MASTER) /* Master Volume */
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.mic_input_volume.vol[0];
				p_transfer_data->num_bytes = 2;
			}
			else if (((p_req_params->setup_packet_wValue & 0xff00) == 0x0200) &&
					((p_req_params->setup_packet_wValue & 0x00ff) <= USB_NUM_AUDIO_CHANNELS))
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.mic_input_volume.vol[(p_req_params->setup_packet_wValue & 0x00ff)-1];
				p_transfer_data->num_bytes = 2;
			}
			else /* Mute */
			{
				p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.mic_input_volume.mute;
				p_transfer_data->num_bytes = 1;
			}
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;

		}
		/* Minimum microphone setting */
		else if (p_req_params->req == CLD_REQ_RANGE)
		{
			user_audio_2_byte_range_resp.w_num_sub_ranges = 1;
			user_audio_2_byte_range_resp.sub_ranges[0].d_max = user_audio_data.mic_input_volume.max;
			user_audio_2_byte_range_resp.sub_ranges[0].d_min = user_audio_data.mic_input_volume.min;
			user_audio_2_byte_range_resp.sub_ranges[0].d_res = user_audio_data.mic_input_volume.resolution;

			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_2_byte_range_resp;
			p_transfer_data->num_bytes = sizeof(user_audio_2_byte_range_resp);
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	/* If the USB Host is Requesting the Clock Source data */
	else if (p_req_params->entity_id == USER_AUDIO_CLOCK_SOURCE_ID_DAC)
	{
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.clock_sample_rate;
			p_transfer_data->num_bytes = 4;
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
		else if (p_req_params->req == CLD_REQ_RANGE)
		{
			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_get_clock_sample_rate_range_resp;
			p_transfer_data->num_bytes = sizeof(user_audio_get_clock_sample_rate_range_resp);
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	else if (p_req_params->entity_id == USER_AUDIO_CLOCK_SOURCE_ID_ADC)
	{
		if (p_req_params->req == CLD_REQ_CURRENT)
		{
			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_data.clock_sample_rate;
			p_transfer_data->num_bytes = 4;
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
		else if (p_req_params->req == CLD_REQ_RANGE)
		{
			p_transfer_data->p_data_buffer = (unsigned char *)&user_audio_get_clock_sample_rate_range_resp;
			p_transfer_data->num_bytes = sizeof(user_audio_get_clock_sample_rate_range_resp);
			p_transfer_data->transfer_timeout_ms = 0;
			p_transfer_data->fp_transfer_aborted_callback = CLD_NULL;
			p_transfer_data->callback.fp_usb_in_transfer_complete = CLD_NULL;
			rv = CLD_USB_TRANSFER_ACCEPT;
		}
	}
	return rv;
}

/**
 * Function called when the Isochronous OUT interface is enabled/disabled by the
 * USB Host using the Set Interface Alternate Setting request.
 *
 * @param enabled - CLD_TRUE = Isochronous OUT endpoint Enabled.
 */
static void user_audio_streaming_rx_endpoint_enabled (CLD_Boolean enabled)
{
	user_audio_data.isochronous_out_enabled = enabled;

	if (enabled == CLD_TRUE)
	{
		first_feedback = CLD_TRUE;
	}
	else
	{
		user_audio_data.rate_feedback_idle = CLD_TRUE;
	}
}

/**
 * Function called when the Isochronous IN interface is enabled/disabled by the
 * USB Host using the Set Interface Alternate Setting request.
 *
 * @param enabled - CLD_TRUE = Isochronous IN endpoint Enabled.
 */
static void user_audio_streaming_tx_endpoint_enabled (CLD_Boolean enabled)
{
	user_audio_data.isochronous_in_enabled = enabled;

	if (enabled != CLD_TRUE)
	{
		user_audio_data.isochronous_in_idle = CLD_TRUE;
	}
}

/*--------------------------------------------
 *  USB_transmit_data
 * 	Alon Slapak 25.1.2019
 * 	Adds the data buffer passed to the function to the head of the microphone circular queue.
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_transmit_audio_stream_data
 *	Input:
 *		�int* p_tx_buffer					- Pointer to the Tx buffer to transmit
 *		unsigned int buffer_size_samples 	- length of buffer
 * 	Output:
 *  	void
 *
 * --------------------------------------------
 */
void USB_transmit_data(int* p_tx_buffer, unsigned int buffer_size_samples)
{
	//--------------------------------------------
	//  Isochronous IN interface is enabled by the USB Host using the Set Interface Alternate Setting request.
	//--------------------------------------------
	if (user_audio_data.isochronous_in_enabled == CLD_TRUE)
	{
		int 			p;
		int 			buffer_size_left_bytes  	= buffer_size_samples * 4;
		int 			index_to_send_bytes 		= 0;
		unsigned char* 	p_tx_buffer_char 			= (unsigned char*)p_tx_buffer;
		//--------------------------------------------
		// 		Segment the TX buffer to USB payloads
		//--------------------------------------------
		for (p = 0; p < buffer_size_samples * 4; p += USB_TX_PAYLOAD_BYTES)
		{
			//--------------------------------------------
			// 		If queue is not full
			//--------------------------------------------
			if (USB_TX_QUEUE_in_index != USB_TX_QUEUE_out_index)
			{
				//--------------------------------------------
				// 		Payload --> queue
				//--------------------------------------------
				USB_TX_QUEUE_pointers[USB_TX_QUEUE_in_index] 			= &p_tx_buffer_char[index_to_send_bytes];
				if (buffer_size_left_bytes > USB_TX_PAYLOAD_BYTES)
				{
					USB_TX_QUEUE_sizes_bytes[USB_TX_QUEUE_in_index] 	= USB_TX_PAYLOAD_BYTES;
					index_to_send_bytes 	+= USB_TX_PAYLOAD_BYTES;
				}
				else
				{
					USB_TX_QUEUE_sizes_bytes[USB_TX_QUEUE_in_index] 	= buffer_size_left_bytes;
				}
				buffer_size_left_bytes -= USB_TX_QUEUE_sizes_bytes[USB_TX_QUEUE_in_index];
				//--------------------------------------------
				// 		Advance IN index (cyclic queue)
				//--------------------------------------------
				USB_TX_QUEUE_in_index = (USB_TX_QUEUE_in_index + 1) % USB_TX_QUEUE_SIZE;
				//--------------------------------------------
				// 		Advance OUT index (cyclic queue)
				//--------------------------------------------
				if (USB_TX_QUEUE_out_index == -1)
				{
					USB_TX_QUEUE_out_index = 0;
				}
			}
		}
		//--------------------------------------------------
		// 	Transmit buffer
		//--------------------------------------------------
//		adi_gpio_SetDirection(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11, ADI_GPIO_DIRECTION_OUTPUT);
//		adi_gpio_Toggle(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11);

		if (user_audio_data.isochronous_in_idle == CLD_TRUE) //TODO: 26.3.2019: do we need this condition?
		{
			USB_tx_audio_data();
		}
	}
}

/*--------------------------------------------
 *  USB_tx_audio_data
 * 	Alon Slapak 31.1.2019
 * 	Transmits the pending audio IN data to the USB Host.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_tx_audio_data
 *	Input:
 *		void
 * 	Output:
 *  	void
 *
 * --------------------------------------------
 */
static void USB_tx_audio_data (void)
{
	static CLD_USB_Transfer_Params audio_data_tx_params;

	adi_rtl_disable_interrupts();

	//-------------------------------------
	// If the Isochronous IN endpoint is enabled and idle
	//-------------------------------------
	if ((user_audio_data.isochronous_in_enabled == CLD_TRUE) && (user_audio_data.isochronous_in_idle == CLD_TRUE))
	{
		//-------------------------------------
		// 	If queue is not empty
		//-------------------------------------
		if (USB_TX_QUEUE_out_index != -1)
		{
			adi_gpio_SetDirection(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11, ADI_GPIO_DIRECTION_OUTPUT);
			adi_gpio_Toggle(ADI_GPIO_PORT_C, ADI_GPIO_PIN_11);

			//-------------------------------------
			// 	If queue is not empty
			//-------------------------------------
			audio_data_tx_params.num_bytes 								= USB_TX_QUEUE_sizes_bytes[USB_TX_QUEUE_out_index];
			audio_data_tx_params.p_data_buffer 							= USB_TX_QUEUE_pointers[USB_TX_QUEUE_out_index];
			audio_data_tx_params.callback.fp_usb_in_transfer_complete 	= USB_data_transmit_completed;
			audio_data_tx_params.transfer_timeout_ms 					= 100;
			audio_data_tx_params.fp_transfer_aborted_callback 			= USB_data_transmit_aborted;
			//-------------------------------------
			// 	Advance out index
			//-------------------------------------
			USB_TX_QUEUE_out_index = (USB_TX_QUEUE_out_index + 1) % USB_TX_QUEUE_SIZE;
			//-------------------------------------
			// 	Queue is empty
			//-------------------------------------
			if (USB_TX_QUEUE_out_index == USB_TX_QUEUE_in_index)
			{
				USB_TX_QUEUE_out_index 	= -1;
				USB_TX_QUEUE_in_index 	= 0;
			}
			//-------------------------------------
			// 	To USB
			//-------------------------------------
			if (cld_sc58x_audio_2_0_lib_transmit_audio_data (&audio_data_tx_params) == CLD_USB_TRANSMIT_SUCCESSFUL)
			{
				user_audio_data.isochronous_in_idle = CLD_FALSE;
			}
		}
	}
	adi_rtl_reenable_interrupts();
}

/*--------------------------------------------
 *  USB_data_transmit_completed
 * 	Alon Slapak 31.1.2019
 * 	Callback: Tx completed
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_tx_audio_data_transmitted
 *	Input:
 *		void
 * 	Output:
 *  	void
 *
 * --------------------------------------------
 */
static void USB_data_transmit_completed (void)
{
	user_audio_data.isochronous_in_idle = CLD_TRUE;
	USB_tx_audio_data();
}

/*--------------------------------------------
 *  USB_data_transmit_aborted
 * 	Alon Slapak 31.1.2019
 * 	Callback: Tx aborted
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_tx_audio_data_aborted
 *	Input:
 *		void
 * 	Output:
 *  	void
 *
 * --------------------------------------------
 */
static void USB_data_transmit_aborted (void)
{
	printf("USB_data_transmit_aborted\n");

	user_audio_data.isochronous_in_idle = CLD_TRUE;
	USB_tx_audio_data();
}



/*--------------------------------------------
 *  USB_event
 * 	Alon Slapak 25.1.2019
 * 	Function Called when a USB event occurs on the USB Audio USB Port.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_audio.c
 *		Function:	user_audio_usb_event
 *	Input:
 *		CLD_USB_Event event - type of event
 *		 	CLD_USB_CABLE_CONNECTED
 *		 	CLD_USB_CABLE_DISCONNECTED
 *		 	CLD_USB_ENUMERATED_CONFIGURED
 *		 	CLD_USB_UN_CONFIGURED
 *		 	CLD_USB_BUS_RESET
 * 	Output:
 *  	void
 * --------------------------------------------
 */
static void USB_event (CLD_USB_Event event)
{
	switch (event)
	{
	case CLD_USB_CABLE_CONNECTED:
		break;
	case CLD_USB_CABLE_DISCONNECTED:
		user_audio_streaming_tx_endpoint_enabled(CLD_FALSE);
		user_audio_streaming_rx_endpoint_enabled(CLD_FALSE);
		break;
	case CLD_USB_ENUMERATED_CONFIGURED:
		break;
	case CLD_USB_UN_CONFIGURED:
		user_audio_streaming_tx_endpoint_enabled(CLD_FALSE);
		user_audio_streaming_rx_endpoint_enabled(CLD_FALSE);
		break;
	case CLD_USB_BUS_RESET:
		user_audio_streaming_tx_endpoint_enabled(CLD_FALSE);
		user_audio_streaming_rx_endpoint_enabled(CLD_FALSE);
		break;
	}
}

/**
 * Function called when the usb feedback endpoint data has been transmitted.
 *
 * @param event - identifies which USB event has occurred.
 */
static void user_audio_feedback_xfr_done (void)
{
	user_audio_data.rate_feedback_idle = CLD_TRUE;
	first_feedback = CLD_FALSE;

	user_audio_tx_feedback_data();
}




/**
 * User defined USB 0 ISR.
 *
 * @param Event, pArg.
 */
static void user_audio_usb0_isr(uint32_t Event, void *pArg)
{
	cld_usb0_isr_callback();
}


/*--------------------------------------------
 *  USB_timer_handler
 * 	Alon Slapak 25.1.2019
 *  Timer ISR to call the cld_time_125us_tick.c every 125 uSec
 *
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		user_adau1979_1962a.c
 *		Function:	user_audio_timer_handler
 *	Input:
 *		void *pCBParam
 *		uint32_t Event
 *		void *pArg
 * 	Output:
 *  	void
 * --------------------------------------------
 */
void USB_timer_handler(void *pCBParam, uint32_t Event, void *pArg)
{
	switch(Event)
	{
	case ADI_TMR_EVENT_DATA_INT:
		/* Call the CLD library 125 microsecond function */
		cld_time_125us_tick();
		break;

	default:
		break;
	}

	return;
}

/**
 * Function called when the CLD library reports a status.
 *
 * @param status_code, p_additional_data, additional_data_size.
 */
static void user_cld_lib_status (unsigned short status_code, void * p_additional_data, unsigned short additional_data_size)
{
	/* Process the CLD library status */
	char * p_str = cld_lib_status_decode(status_code, p_additional_data, additional_data_size);
}




