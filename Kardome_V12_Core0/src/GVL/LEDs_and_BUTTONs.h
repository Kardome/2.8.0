/*
 * LEDs_and_BUTTONs.h
 *
 *  Created on: May 29, 2018
 *      Author: Kardome
 */

#ifndef LEDS_AND_BUTTONS_H_
#define LEDS_AND_BUTTONS_H_

#include <services/gpio/adi_gpio.h>

/*--------------------------------------------
 * 				LEDS
 *--------------------------------------------*/
#define LED_1_PORT               	(ADI_GPIO_PORT_B)
#define LED_2_PORT               	(ADI_GPIO_PORT_B)
#define LED_3_PORT               	(ADI_GPIO_PORT_B)
#define LED_4_PORT               	(ADI_GPIO_PORT_B)

#define LED_1_PIN                	(ADI_GPIO_PIN_11)
#define LED_2_PIN                	(ADI_GPIO_PIN_12)
#define LED_3_PIN                	(ADI_GPIO_PIN_13)
#define LED_4_PIN              		(ADI_GPIO_PIN_14)

/*--------------------------------------------
 * 				Push buttons
 *--------------------------------------------*/
#define GPIO_MEMORY_SIZE 			(ADI_GPIO_CALLBACK_MEM_SIZE*2)
#define PUSH_BUTTON_PORT        	(ADI_GPIO_PORT_F)
#define PUSH_BUTTON_PIN         	(ADI_GPIO_PIN_3)
#define PUSH_BUTTON_PINT         	(ADI_GPIO_PIN_INTERRUPT_4)
#define PUSH_BUTTON_PINT_PIN     	(ADI_GPIO_PIN_0)
#define PUSH_BUTTON_ASSIGN      	(ADI_GPIO_PIN_ASSIGN_PFL_PINT4)
#define PUSH_BUTTON_ASSIGN_BYTE 	(ADI_GPIO_PIN_ASSIGN_BYTE_0)

/*--------------------------------------------
 * 				Functions LEDs
 *--------------------------------------------*/
ADI_GPIO_RESULT 				LED_Init(void);

ADI_GPIO_RESULT 				LED_1_on(void);
ADI_GPIO_RESULT 				LED_2_on(void);
ADI_GPIO_RESULT 				LED_3_on(void);
ADI_GPIO_RESULT 				LED_4_on(void);

ADI_GPIO_RESULT 				LED_1_off(void);
ADI_GPIO_RESULT 				LED_2_off(void);
ADI_GPIO_RESULT 				LED_3_off(void);
ADI_GPIO_RESULT 				LED_4_off(void);

ADI_GPIO_RESULT 				LED_1_toggle(void);
ADI_GPIO_RESULT 				LED_2_toggle(void);
ADI_GPIO_RESULT 				LED_3_toggle(void);
ADI_GPIO_RESULT 				LED_4_toggle(void);
/*--------------------------------------------
 * 				Debug GPIO
 *--------------------------------------------*/
ADI_GPIO_RESULT 				GPIO_debug_toggle(void);
/*--------------------------------------------
 * 				Functions Button
 *--------------------------------------------*/
ADI_GPIO_RESULT 				BUTTON_init(ADI_GPIO_CALLBACK p_BUTTON_callback);






// TODO AS 20.10: add function for error led

#endif /* LEDS_AND_BUTTONS_H_ */
