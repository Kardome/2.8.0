/* * SPORT_TDM.c
 *
 *  Created on: Nov 9, 2018
 *      Author: Kardome
 *
 *  Clocks:
 *      FS:
 *      	The SPORT frame sync (FS) clock is connected to the LRCLK of the ADAU7002 and of the microphones,
 *      	and is the sampling rate in Hz. That is, if the sampling rate is 48kHz, then the LRCLK is 48,000.
 *      	The
 *      CLK:
 *      	the SPORT CLK is connected to the BCLK of the ADAU7002, and is:
 *      			SPORT CLK = SAMPLING_RATE * NUMBER_OF_TDM_SLOTS * SPORT_WORD_LENGTH_BITS.
 *      	Example: when the sampling frequency is 48 kHz, the number of slots is 4 (2 SPORT data channels each 4 TDM slots),
 *      	and the the word-length is 32 bit, we get SPORT CLK = 6.144 MHz.
 *      CLK_DIV:
 *      	The SPORT CLK is derived from the SCLK0, which is SCLK0 frequency = (SYSCLK frequency) / CGU_DIV.S0SEL.
 *      	The default value for CGU_DIV.S0SEL is 2 (HRM FIg 3-7), therefore:
 *      			CLK_DIV = SYSCLK / 2 / (SAMPLING_RATE * (SPORT_NUMBER_OF_TDM_SLOTS / 2) * SPORT_WORD_LENGTH_BITS) - 1
 *      	Example:
 *      		SYSCLK 						= 221.184 MHz
 *      		SAMPLING_RATE 				= 8 kHz
 *      		SPORT_NUMBER_OF_TDM_SLOTS 	= 8
 *      		SPORT_WORD_LENGTH_BITS 		= 32
 *      			==> 	CLK_DIV 		= 107
 *		FS_DIV:
 *			The FS_DIV is the number of sport clock cycles between each frame count, and is:
 *					FS_DIV = (SPORT_NUMBER_OF_TDM_SLOTS / 2) * SPORT_WORD_LENGTH_BITS - 1
 *			Example:
 *				SPORT_NUMBER_OF_TDM_SLOTS 	= 8
 *      		SPORT_WORD_LENGTH_BITS 		= 32
 *      			==> 	FS_DIV 			= 127
 */
#include "SPORT_TDM.h"

unsigned long 			SPORT_DMA_Memory[ADI_SPORT_DMA_MEMORY_SIZE];   /*!< Analog Devices Sport System Services memory */

/*--------------------------------------------
 *  SPORT_TDM_Init
 * 	Alon Slapak 9.11.2018
 * 	Initializes the SPORT in TDM mode.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		adi_adau1979.c
 *		Function:	adi_adau1979_ConfigSport
 *	Input:
 *		void
 * 	Output:
 *  	ADI_SPORT_RESULT:
 *  		ADI_SPORT_SUCCESS
 *			ADI_SPORT_FAILED.
 *
 * --------------------------------------------
 */
ADI_SPORT_RESULT SPORT_TDM_Init(unsigned int sampling_frequency_Hz)
{
	ADI_SPORT_RESULT        		ADI_SPORT_result;
//	unsigned int					SPORT_clk_div;
	/*
	 * -------------------------------
	 *  	Open the SPORT device
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_Open (
			SPORT_DEVICE_NUMBER, 			// SPORT Device instance to be opened (0-7).
			ADI_HALF_SPORT_A,				// Channel ID of the SPORT device( A or B)
			ADI_SPORT_DIR_RX,				// Direction of the SPORT operation. (i.e Rx or Tx)
			ADI_SPORT_MC_MODE, 				// Mode of operation. Indicates in which mode the device is expected to be operated (serial, multi-channel, I2S)
			&SPORT_DMA_Memory,				// Pointer to a 32 bit aligned buffer of size ADI_SPORT_DMA_MEMORY_SIZE required by the driver for the DMA mode.
			ADI_SPORT_DMA_MEMORY_SIZE,		// Size of the buffer to which "pMemory" points
			&hSportDev);					// Pointer to a location where SPORT device handle to be written
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	Enable SPORT DMA
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_EnableDMAMode(
			hSportDev, 						// Device handle to SPORT device
			true); 							// true: Enable the DMA mode. false: Disable the DMA mode. This makes it to work with interrupt mode.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	enable SPORT streaming
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_StreamingEnable(
			hSportDev, 						// Device handle to SPORT device
			true); 							// true: To enable the Streaming by DMA. false: To disable the Streaming by DMA.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	Set DMA Transfer Size
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_SetDmaTransferSize(
			hSportDev, 						// Device handle to SPORT device
			ADI_SPORT_DMA_TRANSFER_4BYTES); // Size of the data transfer. By default, transfer width is set based on the word-length.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}

	/////////////////////////////////////////////////////////////////
	// 		TDMConfig
	/////////////////////////////////////////////////////////////////
	/*
	 * -------------------------------
	 *  	Configure SPORT channel order
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_SelectChannel(
			hSportDev, 						// Device handle to SPORT device
			0, 								// Starting channel number.
			SPORT_NUMBER_OF_TDM_SLOTS - 1); // End channel number.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	 configure the sport for data length, LSB first etc
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_ConfigData(
			hSportDev, 						// Device handle to SPORT device
			ADI_SPORT_DTYPE_SIGN_FILL,		// Specify the data type to be used.
			SPORT_WORD_LENGTH_BITS - 1, 	// Specify the word size of the data. Valid range is from 5(nWordLength = 4) to 32(nWordLength =31).
			false, 							// true: LSB first (Little endian), false: MSB first (Big endian)
			false, 							// true: Enable DMA packing (from 16bit To 32bit). false: disable DMA packing
			false); 						// true: Enable Right Justified mode. false: Disable Right Justified mode.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	Configure the clock for the SPORT. This API set the whether use the internal clock, SPORT clock etc
	 * -------------------------------
	 */
//	SPORT_clk_div = SYSCLK_MAX / 2 / (sampling_frequency_Hz * (SPORT_NUMBER_OF_TDM_SLOTS / 2) * SPORT_WORD_LENGTH_BITS) - 1;
	ADI_SPORT_result = adi_sport_ConfigClock(
			hSportDev, 						// Device handle to SPORT device
			SPORT_CLK_DIV, 					// The value which determines the ratio between System clock and sport clock: SPORT clock = fsclk/( nClockRatio + 1)).
			true, 							// true: Device configured to use Internal clock. false: Device configured to use external clock.
			true, 							// true: Use falling edge of the clock. false: Use rising edge of the clock.
			false); 						// true: Enable gated clock mode. false: Disable gated clock mode.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	Configure the frame sync. This API configure the SPORT whether to use frame sync or not , external or internal framesync etc
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_ConfigFrameSync(
			hSportDev, 						// Device handle to SPORT device
			SPORT_FS_DIV, 					// The value which decides the number of sport clock cycles between each frame count.
			true, 							// true: Device requires a frame sync for its operation. false: Device does not requires a frame sync for its operation
			true, 							// true: Use internal frame sync. false: Use external frame sync
			false, 							//  - Ignored if the device is opened in "receive"(RX) mode -
			false, 							// true: Use active high frame sync. false: Use active low frame sync.
			false, 							// true: Use late frame sync. false: Use Early frame sync.
			false); 		 				// true: Framesync is edge sensitive. false: Framesync is level sensitive.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	Configure the frame sync. This API configure the SPORT whether to use frame sync or not , external or internal framesync etc
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_ConfigMC(
			hSportDev, 						// Device handle to SPORT device
			1u,								// Specify the interval, in number of serial clock cycles between the multichannel frame sync pulse and the first data bit
			SPORT_NUMBER_OF_TDM_SLOTS - 1, 	// Specify the number of multi-channel slots (minmus 1)
			0u, 							// Specify the window offset
			false); 						// true: Enable DMA data packing in Multichannel mode.	false: Disable DMA data packing in Multichannel mode.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}

	return ADI_SPORT_SUCCESS;
}

/*--------------------------------------------
 *  SPORT_TDM_Submit_buffer
 * 	Alon Slapak 27.12.2018
 * 	Submit buffer to the SOPRT Rx.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		adi_adau1979.c
 *		Function:	adi_adau1979_ConfigSport
 *	Input:
 *		pBuffer 			pointer to buffer to submit
 *		buffer_size_BYTE
 * 	Output:
 *  	ADI_SPORT_RESULT:
 *  		ADI_SPORT_SUCCESS
 *			ADI_SPORT_FAILED.
 *
 * --------------------------------------------
 */
ADI_SPORT_RESULT SPORT_TDM_Submit_buffer(void *pBuffer, uint32_t buffer_size_BYTE)
{
		return adi_sport_SubmitBuffer(
				hSportDev, 					// Device handle to SPORT device
				pBuffer,  					// Pointer to buffer from where data need to be transmitted OR to which received data need to to be written.
				buffer_size_BYTE); 			// Size of the data to be transmitted (In bytes)/ received.
}
/*--------------------------------------------
 *  SPORT_TDM_Enable
 * 	Alon Slapak 27.12.2018
 * 	Enables the SPORT Rx channel.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		adi_adau1979.c
 *		Function:	adi_adau1979_ConfigSport
 *	Input:
 *		Enable:
 *			True 	- enable
 *			False 	- disable
 * 	Output:
 *  	ADI_SPORT_RESULT:
 *  		ADI_SPORT_SUCCESS
 *			ADI_SPORT_FAILED.
 *
 * --------------------------------------------
 */
ADI_SPORT_RESULT SPORT_TDM_Enable(bool Enable)
{
	ADI_SPORT_RESULT        ADI_SPORT_result;
	/*
	 * -------------------------------
	 *  	Enable the secondary channel (channel B) of the SPORT
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_EnableSecondary(
			hSportDev, 						// Device handle to SPORT device
			Enable); 						// true: enable channel B(secondary channel) of the SPORT. false: disable channel B(secondary channel) of the SPORT
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	/*
	 * -------------------------------
	 *  	This function Enable/Disable the specified SPORT device.
	 *  	Associated DMA with the device will be enabled if the DMA mode is selected.
	 * -------------------------------
	 */
	ADI_SPORT_result = adi_sport_Enable(
			hSportDev, 						// Device handle to SPORT device
			Enable); 						// true: Enable the device for data transfer. false: Disable the device for data transfer.
	if (ADI_SPORT_result != ADI_SPORT_SUCCESS)
	{
		return ADI_SPORT_result;
	}
	return ADI_SPORT_SUCCESS;
}
/*--------------------------------------------
 *  SPORT_TDM_RegisterCallback
 * 	Alon Slapak 27.12.2018
 *  	Registers a callback function with the SPORT device driver.
 *  	The registered callback function will be called when an buffer is processed OR hardware error(s) encountered.
 * 	Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		adi_adau1979.c
 *		Function:	adi_adau1979_ConfigSport
 *	Input:
 *		pfCallback 			Address of application callback function.
 *		pCBParam 			Parameter passed back to application callback.
 * 	Output:
 *  	ADI_SPORT_RESULT:
 *  		ADI_SPORT_SUCCESS
 *			ADI_SPORT_FAILED.
 *
 * --------------------------------------------
 */
ADI_SPORT_RESULT SPORT_TDM_RegisterCallback(ADI_CALLBACK pfCallback, void *pCBParam)
{
	return adi_sport_RegisterCallback(
			hSportDev, 						// Device handle to SPORT device
			pfCallback, 					// Function pointer to Callback function. Passing a NULL pointer will unregister the callback function.
			NULL); 							// Callback function parameter.
}
