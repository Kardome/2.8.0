/*
 * SPORT_TDM.h
 *
 *  Created on: Nov 9, 2018
 *      Author: Kardome
 *
 */


#ifndef SPORT_TDM_H_
#define SPORT_TDM_H_

#include <drivers/sport/adi_sport.h>
#include "CLOCKS.h"

/*
 * ----------------------------------------
 *  		Defines
 *-----------------------------------------
 */
#define		SPORT_DEVICE_NUMBER 		0u 		 	// SPORT Device instance to be opened (0-7).
#define		SPORT_WORD_LENGTH_BITS 		32u 		// Specify the word size of the data. Valid range is from 5 to 32.
#define 	SPORT_NUMBER_OF_TDM_SLOTS 	8u 			// Specify the number of multi-channel (MC) slots
#define 	SPORT_CLK_DIV 				17 			// The value which determines the ratio between System clock and sport clock: SPORT clock = fsclk/( nClockRatio + 1)).
#define 	SPORT_FS_DIV 				127 		// The value which decides the number of sport clock cycles between each frame count.
/*
 * ----------------------------------------
 *  		Variables
 *-----------------------------------------
 */
ADI_SPORT_HANDLE                hSportDev;      /* SPORT Device handle allocated to the DAC Serial data port */


/*
 * ----------------------------------------
 *  		Functions
 *-----------------------------------------
 */
ADI_SPORT_RESULT 	SPORT_TDM_Init(unsigned int sampling_frequency_Hz);
ADI_SPORT_RESULT 	SPORT_TDM_Submit_buffer(void *pBuffer, uint32_t buffer_size_BYTE);
ADI_SPORT_RESULT 	SPORT_TDM_Enable(bool Enable);
ADI_SPORT_RESULT 	SPORT_TDM_RegisterCallback(ADI_CALLBACK pfCallback, void *pCBParam);

#endif /* SPORT_TDM_H_ */
