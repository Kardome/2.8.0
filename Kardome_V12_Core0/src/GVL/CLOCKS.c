/*
 * CLOCKS.c
 *
 *  Created on: Dec 7, 2018
 *      Author: Kardome
 */

#include "CLOCKS.h"


unsigned char TimerMemory[ADI_TMR_MEMORY];

/*--------------------------------------------
 *  CLOCKS_Init
 * 	Alon Slapak 29.10.2018
 * 	Initializes the clocks of the microphones.
 *	Input:
 *		void
 * 	Output:
 *  	ADI_GPIO_RESULT:
 *  		ADI_GPIO_SUCCESS
 *  		ADI_GPIO_FAILURE
 *
 * --------------------------------------------
 */
ADI_GPIO_RESULT CLOCKS_Init(void)
{
	ADI_GPIO_RESULT 	GPIO_result;
	//-----------------------------------
	//		Enable BCLK 0-4
	//-----------------------------------
	GPIO_result = adi_gpio_SetDirection(BCLK_ENABLE_1_PORT, BCLK_ENABLE_1_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= adi_gpio_Clear(BCLK_ENABLE_1_PORT, BCLK_ENABLE_1_PIN);
	GPIO_result |= adi_gpio_Set(BCLK_ENABLE_1_PORT, BCLK_ENABLE_1_PIN);
	//-----------------------------------
	//		Enable BCLK 5 - test point for debug
	//-----------------------------------
	GPIO_result = adi_gpio_SetDirection(BCLK_ENABLE_2_PORT, BCLK_ENABLE_2_PIN, ADI_GPIO_DIRECTION_OUTPUT);
	GPIO_result |= adi_gpio_Clear(BCLK_ENABLE_2_PORT, BCLK_ENABLE_2_PIN);
	GPIO_result |= adi_gpio_Set(BCLK_ENABLE_2_PORT, BCLK_ENABLE_2_PIN);


	return GPIO_result;
}


/*--------------------------------------------
 *  CLOCKS_timer_init
 * 	Alon Slapak 25.1.2019
 * 	Initializes the timer of the USB to call the cld_time_125us_tick.c
 * 	using interrupt every 125 uSec, that is 8000 Hz.
 *  Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		main
 *		Function:	timer_init.c
 *	Input:
 *		ADI_CALLBACK pfCallback
 * 	Output:
 *  	ADI_TMR_RESULT:
 *   		ADI_TMR_SUCCESS
 *   		ADI_TMR_FAILURE
 * --------------------------------------------
 */
ADI_TMR_RESULT CLOCKS_timer_init(ADI_CALLBACK pfCallback)
{
    ADI_TMR_HANDLE   hTimer;
    ADI_TMR_RESULT   TMR_result;

    /* Open the timer */
    if ((TMR_result = adi_tmr_Open (0, 		// Timer number to open.
                        TimerMemory, 		// Pointer to the memory which is required to operate the timer instance.
                        ADI_TMR_MEMORY, 	// Size of the given memory in bytes.
						pfCallback, 		// Pointer to application callback function to notify events.
                        NULL, 				// Pointer to callback parameter which will be passed back to the application when callback function is called.
                        &hTimer 			// Pointer to a location where the handle to the opened timer is written.
						)) != ADI_TMR_SUCCESS)
     {
         return TMR_result;
     }

    /* Set the mode to PWM OUT */
    if ((TMR_result = adi_tmr_SetMode(hTimer, ADI_TMR_MODE_CONTINUOUS_PWMOUT)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    /* Set the IRQ mode to get interrupt after timer counts to Delay + Width */
    if ((TMR_result = adi_tmr_SetIRQMode(hTimer, ADI_TMR_IRQMODE_WIDTH_DELAY)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    /* Set the Period */
    if ((TMR_result = adi_tmr_SetPeriod(hTimer, TIMER_PERIOD)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    /* Set the timer width */
    if ((TMR_result = adi_tmr_SetWidth(hTimer, TIMER_WIDTH)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    /* Set the timer delay */
    if ((TMR_result = adi_tmr_SetDelay(hTimer, 100)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    /* Enable the timer */
    if ((TMR_result = adi_tmr_Enable(hTimer, true)) != ADI_TMR_SUCCESS)
    {
    	return TMR_result;
    }
    return TMR_result;
}

/*--------------------------------------------
 *  CLOCKS_power_and_system_clock_init
 * 	Alon Slapak 25.1.2019
 * 	Initializes ADI power system service, Core clock and system clock.
 * 	Core clock is 442.368 MHz and system clock is 221.184 MHz, so that
 * 	the SPORT can produce accurate BCLK of 6.144 MHz. Therefore, the clock
 * 	is extracted from a 24.576 MHz xtal.
 *
 *  Reference:
 * 		Project: 	CLD_Audio_2_0_8ch_Ex_v1_00_core0
 *		File: 		main
 *		Function:	PowerServiceInit.c
 *	Input:
 *		void
 * 	Output:
 *  	ADI_PWR_RESULT:
 *   		ADI_PWR_SUCCESS
 *   		ADI_PWR_FAILURE
 * --------------------------------------------
 */
ADI_PWR_RESULT CLOCKS_power_and_system_clock_init(void)
{
	ADI_PWR_RESULT   PWR_result;

    if ((PWR_result = adi_pwr_Init(CGU_DEV, CLKIN)) != ADI_PWR_SUCCESS)
    {
        return PWR_result;
    }
    if ((PWR_result = adi_pwr_SetPowerMode(CGU_DEV, ADI_PWR_MODE_FULL_ON)) != ADI_PWR_SUCCESS)
    {
    	 return PWR_result;
    }
    if((PWR_result = adi_pwr_SetFreq(CGU_DEV, CORE_MAX, SYSCLK_MAX)) != ADI_PWR_SUCCESS)
    {
    	return PWR_result;
    }
    return ADI_PWR_SUCCESS;
}



