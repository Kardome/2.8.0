#ifndef __USER_AUDIO_W_CDC__
#define __USER_AUDIO_W_CDC__
/*==============================================================================
    FILE:           user_audio_w_cdc.h

    DESCRIPTION:    Uses the cld_sc58x_audio_2_0_w_cdc_lib library to implement a basic
                    USB audio 2.0 and CDC/ACM device.

    Copyright (c) 2018 Closed Loop Design, LLC

    This software is supplied "AS IS" without any warranties, express, implied
    or statutory, including but not limited to the implied warranties of fitness
    for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
    extends you a royalty-free right to use, reproduce and distribute this source
    file as well as executable files created using this source file for use with 
    Analog Devices SC5xx family processors only. Nothing else gives you 
    the right to use this source file.

==============================================================================*/
/*!
 * @file      user_audio_w_cdc.h
 * @brief     Uses the cld_sc58x_audio_2_0_w_cdc_lib library to implement a basic
 *            USB audio 2.0 and CDC/ACM device.
 *
 * @details
 *            User defined interface with the cld_sc58x_audio_2_0_w_cdc_lib library to
 *            implement a custom USB Audio 2.0 and CDC device.
 *
 *            Copyright (c) 2018 Closed Loop Design, LLC
 *
 *            This software is supplied "AS IS" without any warranties, express, implied
 *            or statutory, including but not limited to the implied warranties of fitness
 *            for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
 *            extends you a royalty-free right to use, reproduce and distribute this source
 *            file as well as executable files created using this source file for use with 
 *            Analog Devices SC5xx family processors only. Nothing else gives you 
 *            the right to use this source file.
 *
 */

#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <services/tmr/adi_tmr.h>
#include "cld_sc58x_audio_2_0_lib.h"

/*------------------------------------------------
 *  		Defines
 *------------------------------------------------*/
#define USB_NUM_AUDIO_CHANNELS 			8
#define USB_BIT_RESOLUTION 				32
/*
 * Vendor_ID and product_ID are licenced by Thesycon 8. August 2018
 */
#define USB_VENDOR_ID					0x152A
#define USB_PRODUCT_ID           		0x86FD


/* ARM processor interrupt request IDs. */
#define ADI_INTR_BASE             		32         /* ARM A5 GIC offset needed for interrupt indexing for ADSP-SC58x */
#define USB0_STAT_IRQn          		(ADI_INTR_BASE + 132)    /*!< Status/FIFO Data Ready */
#define USB0_DATA_IRQn          		(ADI_INTR_BASE + 133)    /*!< DMA Status/Transfer Complete */

/**
  * Feature Unit ID used to control the Headphone volume
  */
#define USER_AUDIO_HEADPHONE_FEATURE_UNIT_ID                    0x09

/**
 * Control and Channel number from the Setup Packet wValue field of
 * the Headphone Master channel
 */
#define USER_AUDIO_HEADPHONE_CONTROL_AND_CHANNEL_NUM_MASTER     0x0200

/**
 * Feature Unit ID used to control the Microphone volume
 */
#define USER_AUDIO_MICROPHONE_FEATURE_UNIT_ID                   0x0a
/**
 *Control and Channel number from the Setup Packet wValue field of
 * the Microphone volume
 */
#define USER_AUDIO_MICROPHONE_CONTROL_AND_CHANNEL_NUM_MASTER    0x0200

/**
 * Clock Source ID connected to the headphones and microphone Units
 */
#define USER_AUDIO_CLOCK_SOURCE_ID_DAC                              0x03
#define USER_AUDIO_CLOCK_SOURCE_ID_ADC                              0x04

/**
 * High Speed max packet size should be large enough to hold 1 bInterval worth
 * of audio samples.
 */
#define USER_AUDIO_MAX_PACKET_SIZE                              512
#define USB_PC_DATA_BUFFER_LENGTH 								256
#define USB_PC_DATA_BUFFER_SIGNATURE							12345 //0x12345678
#define USB_INTEGER_ACCURACY_MASK 								0xffffff00
#define USB_ACKNOWLEDGE_MESSAGE 								224466 //0x12345678


//--------------------------------------
// 		USB Tx
//--------------------------------------
#define USB_TX_QUEUE_SIZE 										50


/*------------------------------------------------
 *  		Structures
 *------------------------------------------------*/
typedef struct
{
	bool 			FLAG_loopback;
	unsigned int 	SPORT_buffer_length_samples;
	unsigned int 	Sampling_frequency_Hz;
} T_USB_configuration;
/*------------------------------------------------
 *  		ENUM results
 *------------------------------------------------*/
typedef enum
{
	USB_INIT_SUCCESS = 0,    /*!< Initialization successful */
    USB_INIT_ONGOING,        /*!< Initialization in process */
    USB_INIT_FAILED,         /*!< Initialization failed */
} USB_RESULT;
/*------------------------------------------------
 *  		Functions
 *------------------------------------------------*/
extern USB_RESULT 	USB_init (T_USB_configuration* USB_configuration);
extern void 		USB_main (void);
extern void 		USB_transmit_data(int* p_tx_buffer, unsigned int buffer_size_samples);
extern void 		USB_timer_handler(void *pCBParam, unsigned long Event, void *pArg);

#endif /* __USER_AUDIO_W_CDC__ */
