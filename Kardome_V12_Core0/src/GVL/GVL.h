#ifndef __USER_ADAU1761__
#define __USER_ADAU1761__
/*==============================================================================
    FILE:           GVL.h

    DESCRIPTION:    Provides a interface between the user_audio USB Audio 2.0
                    functionality and the ADI adau1979 and asau1962a drivers.

    Copyright (c) 2018 Closed Loop Design, LLC

    This software is supplied "AS IS" without any warranties, express, implied
    or statutory, including but not limited to the implied warranties of fitness
    for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
    extends you a royalty-free right to use, reproduce and distribute this source
    file as well as executable files created using this source file for use with 
    Analog Devices SC5xx family processors only. Nothing else gives you 
    the right to use this source file.

==============================================================================*/
/*!
 * @file      GVL.h
 * @brief     Interface logic for the ADAU1979 and ADAU1962a drivers.
 *
 * @details
 *            Provides an interface between the user_audio USB Audio 2.0
 *            functionality and the ADI adau1979 and adau1962a drivers.
 *
 *            Copyright (c) 2018 Closed Loop Design, LLC
 *
 *            This software is supplied "AS IS" without any warranties, express, implied
 *            or statutory, including but not limited to the implied warranties of fitness
 *            for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
 *            extends you a royalty-free right to use, reproduce and distribute this source
 *            file as well as executable files created using this source file for use with 
 *            Analog Devices SC5xx family processors only. Nothing else gives you 
 *            the right to use this source file.

 *
 */

#include <services/spu/adi_spu.h>
#include <services/gpio/adi_gpio.h>
#include <services/dma/adi_dma.h>
#include <services/int/adi_int.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "cld_sc58x_audio_2_0_lib.h"
#include "USB.h"
#include "LEDs_and_BUTTONs.h"
#include "SPORT_TDM.h"
#include "CLOCKS.h"


#include "..\GVL_parameters.h"


/*---------------------------------------------------
 * 		Defines
 *----------------------------------------------------*/
#define GVL_SAMPLING_RATE_HZ 	        48000 //TODO: 26.3.2019: remove??

/*------------------------------------------------
 *  		Structures
 *------------------------------------------------*/
typedef struct
{
	ADI_GPIO_CALLBACK 	p_BUTTON_callback; 				// Pointer to callback function of push button
	bool 			 	FLAG_loopback;					// Flag for loopback (true) or microphones (false)
	unsigned int 		SPORT_buffer_length_samples; 	// SPORT buffer length (up to 1024)
	unsigned int 		Sampling_frequency_Hz; 			// THe sampling frequency (4,8,16,48 kHz)
} T_GVL_configuration;
/*---------------------------------------------------
 * 		 Enumeration of return codes for the GVL_init function.
 *----------------------------------------------------*/
typedef enum
{
    GVL_SUCCESS = 0,
	GVL_ONGOING,
	GVL_FAILED,
	GVL_INIT_POWER_AND_SYSTEM_CLOCK_FAILED,
	GVL_INIT_TIMER_FAILED,
	GVL_INIT_USB_FAILED,
	GVL_INIT_LED_FAILED,
	GVL_INIT_CLOCK_BUFFER_FAILED,
	GVL_INIT_BUTTON_FAILED,
	GVL_SPORT_INIT_FAILED,
	GVL_SPORT_REGISTER_CALLBACK_FAILED,
	GVL_SPORT_SUBMIT_BUFFERS_FAILED,
	GVL_SPORT_ENABLE_FAILED,
} GVL_RESULT;
/*---------------------------------------------------
 * 		Functions (GVL API)
 *----------------------------------------------------*/
GVL_RESULT 		GVL_init(T_GVL_configuration* GVL_configuration);
GVL_RESULT 		GVL_main(void);

GVL_RESULT 		GVL_LED_on(uint32_t led_number);
GVL_RESULT 		GVL_LED_off(uint32_t led_number);
GVL_RESULT 		GVL_LED_toggle(uint32_t led_number);
GVL_RESULT 		GVL_GPIO_debug_toggle(void);

/*---------------------------------------------------
 * 		Externs
 *----------------------------------------------------*/
extern int 		GVL_microphone_enable;

#endif /* __USER_ADAU1761__ */
