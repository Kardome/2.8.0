/*
 * CLOCKS.h
 *
 *  Created on: Dec 7, 2018
 *      Author: Kardome
 */

#ifndef GVL_CLOCKS_H_
#define GVL_CLOCKS_H_

#include <services/gpio/adi_gpio.h>
#include <services/tmr/adi_tmr.h>
#include <services/pwr/adi_pwr.h>
#include <string.h>

//------------------------------------------
// 		GPIO for the SPORT BCLK buffer
//------------------------------------------
#define BCLK_ENABLE_1_PORT 				(ADI_GPIO_PORT_D)
#define BCLK_ENABLE_1_PIN 				(ADI_GPIO_PIN_5)

#define BCLK_ENABLE_2_PORT       		(ADI_GPIO_PORT_D)
#define BCLK_ENABLE_2_PIN       		(ADI_GPIO_PIN_4)
//------------------------------------------
// 		Core clock and system clock
//------------------------------------------
#define CGU_DEV                 (0)             			/* Clock Generation Unit device number. */
#define MHZ                     (1000000u)      			/* MHz multiplier */
#define CLKIN                   (24.576 * MHZ)     			/* 24.576 MHz clock-in */
#define CORE_MAX                (CLKIN * 18u)    			/* 442.368 MHz Core Clock */
#define SYSCLK_MAX              (CLKIN * 9u)    			/* 221.184 MHz SysClk */

#define TIMER_PERIOD            (SYSCLK_MAX / 8000 / 2)   	/* Divider to obtain interrupt every 125uSec */
#define TIMER_WIDTH             (TIMER_PERIOD / 2)       	/* half pulse width*/


ADI_GPIO_RESULT 				CLOCKS_Init(void);
ADI_TMR_RESULT 					CLOCKS_timer_init(ADI_CALLBACK pfCallback);
ADI_PWR_RESULT 					CLOCKS_power_and_system_clock_init(void);

#endif /* GVL_CLOCKS_H_ */
