/*
 * GVL_parameters.h
 *
 *  Created on: Mar 5, 2019
 *      Author: Kardome
 */

#ifndef GVL_PARAMETERS_H_
#define GVL_PARAMETERS_H_

//TODO: as 22.3.2019 remove the SPORT_BUFFER_LENGTH_SAMPLES from the code. Use only the configuration structure.
#define SPORT_BUFFER_LENGTH_SAMPLES 				256

#define GVL_NUM_AUDIO_CHANNELS          			8
#define GVL_SAMPLE_SIZE_BYTES      					4
#define GVL_MAX_BUFFER_SIZE_PER_CHANNEL_SAMPLES 	1024
#define GVL_MAX_BUFFER_SIZE_BYTES 					GVL_MAX_BUFFER_SIZE_PER_CHANNEL_SAMPLES * GVL_NUM_AUDIO_CHANNELS * GVL_SAMPLE_SIZE_BYTES
#define GVL_MAX_BUFFER_SIZE_SAMPLES 				GVL_MAX_BUFFER_SIZE_PER_CHANNEL_SAMPLES * GVL_NUM_AUDIO_CHANNELS
#define	GVL_MAX_PAREMETERS_FROM_PC_BYTES 			4096 * 8 * 4
#define USB_TX_PAYLOAD_BYTES						480



/*---------------------------------------------
 *
 *  			MEMORY
 *
 * 			Buffers uncached section (MEM_L3_UNCACHED. See adsp-sc589.ld)
 *
 * 			Buffer 						Size (Bytes)
 * 			======						============
 * 	GVL_RX_from_SPORT_PING 				GVL_MAX_BUFFER_SIZE_BYTES
 *  GVL_RX_from_SPORT_PONG 				GVL_MAX_BUFFER_SIZE_BYTES
 *  GVL_TX_to_USB_PING					GVL_MAX_BUFFER_SIZE_BYTES
 *  GVL_TX_to_USB_PONG 					GVL_MAX_BUFFER_SIZE_BYTES
 * 	GVL_RX_from_USB_PING				GVL_MAX_BUFFER_SIZE_BYTES + USB_TX_PAYLOAD_BYTES
 * 	GVL_RX_from_USB_PONG				GVL_MAX_BUFFER_SIZE_BYTES + USB_TX_PAYLOAD_BYTES
 * 	USB_SHARC_PC_data_buffer 			GVL_MAX_PAREMETERS_FROM_PC_BYTES
 *
 * 	Note: The parentheses around the addresses are required to avoid scaling of the shift in casting !
 *---------------------------------------------
 */
#define ADDRESS_UNCACHED_BASE 	 		(0xcf000000)

#define ADDRESS_RX_from_SPORT_PING 	 	(ADDRESS_UNCACHED_BASE 			+ GVL_MAX_BUFFER_SIZE_BYTES)
#define	ADDRESS_RX_from_SPORT_PONG 		(ADDRESS_RX_from_SPORT_PING 	+ GVL_MAX_BUFFER_SIZE_BYTES)
#define	ADDRESS_TX_to_USB_PING 			(ADDRESS_RX_from_SPORT_PONG 	+ GVL_MAX_BUFFER_SIZE_BYTES)
#define	ADDRESS_TX_to_USB_PONG 			(ADDRESS_TX_to_USB_PING 		+ GVL_MAX_BUFFER_SIZE_BYTES)
#define	ADDRESS_RX_from_USB_PING		(ADDRESS_TX_to_USB_PONG 		+ GVL_MAX_BUFFER_SIZE_BYTES)
#define	ADDRESS_RX_from_USB_PONG		(ADDRESS_RX_from_USB_PING		+ GVL_MAX_BUFFER_SIZE_BYTES + USB_TX_PAYLOAD_BYTES)
#define ADDRESS_PC_data_buffer 			(ADDRESS_RX_from_USB_PONG		+ GVL_MAX_BUFFER_SIZE_BYTES + USB_TX_PAYLOAD_BYTES)


#endif /* GVL_PARAMETERS_H_ */
