/*=============================================================================
    FILE:           main.c

    DESCRIPTION:    CLD Audio v2.0 8 channel In/Out example mainline file.

    Copyright (c) 2018 Closed Loop Design, LLC

    This software is supplied "AS IS" without any warranties, express, implied
    or statutory, including but not limited to the implied warranties of fitness
    for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
    extends you a royalty-free right to use, reproduce and distribute this source
    file as well as executable files created using this source file for use with 
    Analog Devices SC5xx family processors only. Nothing else gives you 
    the right to use this source file.

==============================================================================*/
/*!
 * @file      main.c
 * @brief     CLD Audio v2.0 8 channel In/Out with CDC/ACM example mainline file.
 *
 * @details
 *            This file contains mainline state machine used to implement a
 *            8 channel In/Out with CDC/ACM example using the cld_sc58x_audio_2_0_w_cdc_lib library on
 *            a SC589 Cortex-A5 processor.
 *
 *            Copyright (c) 2018 Closed Loop Design, LLC
 *
 *            This software is supplied "AS IS" without any warranties, express, implied
 *            or statutory, including but not limited to the implied warranties of fitness
 *            for purpose, satisfactory quality and non-infringement. Closed Loop Design LLC
 *            extends you a royalty-free right to use, reproduce and distribute this source
 *            file as well as executable files created using this source file for use with 
 *            Analog Devices SC5xx family processors only. Nothing else gives you 
 *            the right to use this source file.
 *
 */
#include <sys/platform.h>
//#include <sys/adi_core.h>
#include <adi/sys/adi_core.h>

#include "adi_initialize.h"
#include <string.h>

#include "GVL/GVL.h"
#include "GVL_parameters.h"
/**
 * @brief Example callback function for the push-button
 * @details This callback is invoked when the push-button is pressed
 *
 */
void PB_callback()
{
	GVL_LED_toggle(3);
}


/**
 * @brief Example project main() function.
 * @details Mainline Function for the project.
 */

int main()
{
	GVL_RESULT 				GVL_result;
	T_GVL_configuration		GVL_configuration;


	/* auto-generated code */
//	adi_initComponents();
	/* Enable Cores 1 and 2. */
	adi_core_enable(ADI_CORE_SHARC0);
	adi_core_enable(ADI_CORE_SHARC1);
	/*----------------------------------------
	 * 	GVL configuration
	 *----------------------------------------*/
	GVL_configuration.p_BUTTON_callback 			= (ADI_GPIO_CALLBACK)&PB_callback;
	GVL_configuration.FLAG_loopback 				= false;
	GVL_configuration.SPORT_buffer_length_samples  	= SPORT_BUFFER_LENGTH_SAMPLES;
	GVL_configuration.Sampling_frequency_Hz 		= 48000;
	/*----------------------------------------
	 * 	Initialize the GVL library
	 *----------------------------------------*/
	if ((GVL_result = GVL_init(&GVL_configuration)) != GVL_SUCCESS)
	{
			// TODO 25.1.2019: ERROR LED
	}
	/*----------------------------------------
	 * 	Main loop
	 *----------------------------------------*/
	while (1)
	{
		if ((GVL_result = GVL_main()) != GVL_SUCCESS)
		{
			// TODO 25.1.2019: ERROR LED
		}
	}
	return 0;
}


