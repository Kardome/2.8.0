################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/apt-sc589.c \
../src/main_Core0.c 

SRC_OBJS += \
./src/apt-sc589.o \
./src/main_Core0.o 

C_DEPS += \
./src/apt-sc589.d \
./src/main_Core0.d 


# Each subdirectory must supply rules for building sources it contributes
src/apt-sc589.o: ../src/apt-sc589.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/apt-sc589.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/main_Core0.o: ../src/main_Core0.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/main_Core0.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


