################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/GVL/CLOCKS.c \
../src/GVL/GVL.c \
../src/GVL/LEDs_and_BUTTONs.c \
../src/GVL/SPORT_TDM.c \
../src/GVL/USB.c 

SRC_OBJS += \
./src/GVL/CLOCKS.o \
./src/GVL/GVL.o \
./src/GVL/LEDs_and_BUTTONs.o \
./src/GVL/SPORT_TDM.o \
./src/GVL/USB.o 

C_DEPS += \
./src/GVL/CLOCKS.d \
./src/GVL/GVL.d \
./src/GVL/LEDs_and_BUTTONs.d \
./src/GVL/SPORT_TDM.d \
./src/GVL/USB.d 


# Each subdirectory must supply rules for building sources it contributes
src/GVL/CLOCKS.o: ../src/GVL/CLOCKS.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/GVL/CLOCKS.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GVL/GVL.o: ../src/GVL/GVL.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/GVL/GVL.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GVL/LEDs_and_BUTTONs.o: ../src/GVL/LEDs_and_BUTTONs.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/GVL/LEDs_and_BUTTONs.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GVL/SPORT_TDM.o: ../src/GVL/SPORT_TDM.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/GVL/SPORT_TDM.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GVL/USB.o: ../src/GVL/USB.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore ARM Bare Metal C Compiler'
	arm-none-eabi-gcc -g -gdwarf-2 -ffunction-sections -fdata-sections -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5dbb807cc9caf917e5fcd6c2fcd5399f.txt -Wall -c -mproc=ADSP-SC589 -msi-revision=1.0 -MMD -MP -MF"src/GVL/USB.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


